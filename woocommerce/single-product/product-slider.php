<?php

/**
 * Single Product Slider
 *
 * @package TEN
 */

use TEN\WooCommerce\Product\ProductMetaInfluences;
use TEN\WooCommerceInit;

defined( 'ABSPATH' ) || exit;

// Note: `wc_get_gallery_image_html` was added in WC 3.3.2 and did not exist prior. This check protects against theme overrides being used on older versions of WC.
if ( ! function_exists( 'wc_get_gallery_image_html' ) ) {
	return;
}

global $product;

if ( $product->is_type( 'variable' ) ) {
	$variations = $product->get_available_variations();
}

$post_thumbnail_id = $product->get_image_id();
$attachment_ids    = $product->get_gallery_image_ids();
$product_meta      = new ProductMetaInfluences();
$meta              = $product_meta->get_product_meta();

if ( $product->is_type( 'variable' ) ) {
	$attachment_ids = WooCommerceInit::get_image_ids( $variations, $attachment_ids );
}

if ( count( $attachment_ids ) > 1 ) :
	?>
	<div
			class="product-slider"
			data-title="<?php echo isset( $meta['influencer'][0]['id'] ) ? esc_html( get_the_title( $meta['influencer'][0]['id'] ) ) : ''; ?>"
			style="color:<?php echo ! empty( $meta['color'] ) ? esc_attr( $meta['color'] ) : '#A1B8DF'; ?>;">
		<?php foreach ( $attachment_ids as $key => $attachment_id ) : ?>
			<div class="item">
				<a href="<?php echo esc_url( wp_get_attachment_image_url( $attachment_id, 'full' ) ) ?>"
				   data-fancybox="gallery">
					<?php
					echo wp_kses_post(
						sprintf(
							'<img src="%s" alt="%s" data-vaiastion-image-id="%d">',
							esc_url( wp_get_attachment_image_url( $attachment_id, 'full' ) ),
							esc_html( get_the_title( $attachment_id ) ),
							esc_attr( $attachment_ids[ $key ] )
						)
					);
					?>
				</a>
			</div>
		<?php endforeach; ?>
	</div>
<?php
else:

	?>
	<div
			class="product-slider"
			data-title="<?php echo esc_html( get_the_title( $meta['influencer'][0]['id'] ) ); ?>"
			style="color:<?php echo esc_attr( $meta['color'] ); ?>;">
		<div class="item">
			<a href="<?php echo esc_url( wp_get_attachment_image_url( $product->get_image_id(), 'full' ) ) ?>"
			   data-fancybox="gallery">
				<?php
				echo wp_kses_post(
					sprintf(
						'<img src="%s" alt="%s">',
						esc_url( wp_get_attachment_image_url( $product->get_image_id(), 'full' ) ),
						esc_html( get_the_title( $product->get_image_id() ) ),
					)
				);
				?>
			</a>
		</div>
	</div>
<?php
endif;