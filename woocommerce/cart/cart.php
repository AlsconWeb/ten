<?php
/**
 * Cart Page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/cart.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.8.0
 */

use TEN\WooCommerce\Cart\ChangeCart;

defined( 'ABSPATH' ) || exit;
?>
<div class="row align-items-center">
	<?php do_action( 'woocommerce_before_cart' ); ?>

	<div class="col-8">
		<?php
		$cart = new ChangeCart();
		$cart->show_cart_header();
		?>
		<form class="woocommerce-cart-form" action="<?php echo esc_url( wc_get_cart_url() ); ?>" method="post">
			<?php do_action( 'woocommerce_before_cart_table' ); ?>
			<div class="product-items">
				<?php foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) : ?>
					<div class="product-item">
						<?php
						$_product   = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
						$product_id = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );

						if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
							$product_permalink = apply_filters( 'woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink( $cart_item ) : '', $cart_item, $cart_item_key );
							?>
							<?php
							$image_id = $_product->get_image_id();
							if ( ! empty( $image_id ) ) {
								echo sprintf(
									'<img src="%s" alt="%s">',
									esc_url( wp_get_attachment_image_url( $image_id, 'full' ) ),
									esc_html( get_the_title( $image_id ) )
								);
							} else {
								echo '<img src="https://via.placeholder.com/300" alt="No image">';
							}
							?>
							<div class="description">
								<h3 class="title">
									<?php
									if ( $_product->is_type( 'variation' ) ) {
										echo esc_html( get_the_title( $_product->get_parent_id() ) );
									}

									if ( ! $_product->is_type( 'variation' ) ) {
										echo esc_html( get_the_title( $_product->get_id() ) );
									}
									?>
								</h3>

								<?php
								if ( $_product->is_type( 'variation' ) ) {

									$attributes = $_product->get_attributes();

									echo sprintf(
										'<p class="size">%s – <span>%s</span></p>',
										esc_html( __( 'Rozmiar', 'ten' ) ),
										esc_attr( strtoupper( $attributes['pa_sizes'] ) ),
									);

									echo sprintf(
										'<p class="color">%s <span style="color:%s;"></span></p>',
										esc_html( __( 'Kolor', 'ten' ) ),
										esc_attr( $cart->get_color_product( $attributes['pa_color'] ) )
									);
								}
								?>
								<div class="number">
									<?php
									if ( $_product->is_sold_individually() ) {
										$product_quantity = sprintf( '1 <input type="hidden" name="cart[%s][qty]" value="1" />', $cart_item_key );
									} else {
										$product_quantity = woocommerce_quantity_input(
											[
												'input_name'   => "cart[{$cart_item_key}][qty]",
												'input_value'  => $cart_item['quantity'],
												'max_value'    => $_product->get_max_purchase_quantity(),
												'min_value'    => '1',
												'product_name' => $_product->get_name(),
											],
											$_product,
											false
										);
									}

									echo apply_filters( 'woocommerce_cart_item_quantity', $product_quantity, $cart_item_key, $cart_item ); // PHPCS: XSS ok.
									?>
								</div>
								<?php echo apply_filters( 'woocommerce_cart_item_price', WC()->cart->get_product_price( $_product ), $cart_item, $cart_item_key ); // PHPCS: XSS ok. ?>
							</div>
							<?php
							echo apply_filters( // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
								'woocommerce_cart_item_remove_link',
								sprintf(
									'<a href="%s" class="remove" aria-label="%s" data-product_id="%s" data-product_sku="%s"><i class="icon-close remove">%s</i></a>',
									esc_url( wc_get_cart_remove_url( $cart_item_key ) ),
									esc_html__( 'Remove this item', 'woocommerce' ),
									esc_attr( $product_id ),
									esc_attr( $_product->get_sku() ),
									esc_html( __( 'usuń', 'ten' ) )
								),
								$cart_item_key
							);
							?>
							<?php
						}
						?>

					</div>
				<?php endforeach; ?>

				<?php
				echo wp_kses_post(
					sprintf(
						'<p class="gifts">%s <a href="%s" data-quantity="%s" type="submit" class="%s" data-product_id="%d" rel="nofollow">%s</a> %s</p>',
						__( 'Gratulacje! Dostałeś możliwość zakupu', 'ten' ),
						esc_url( '?add-to-cart=' . 690 . '&quantity=1' ),
						1,
						esc_attr( 'add_to_cart_button' ),
						690,
						/* translators: %s: button text */
						__( 'kalendarza Ten Fajny Management', 'ten' ),
						__( 'już za 1 zł.', 'ten' )
					)
				);
				?>
			</div>
			<?php if ( wc_coupons_enabled() ) { ?>
				<div class="coupon">
					<input
							type="text"
							name="coupon_code"
							class="input-text"
							id="coupon_code"
							value=""
							placeholder="<?php esc_attr_e( 'Kod rabatowy', 'ten' ); ?>"/>
					<button
							type="submit"
							class="button icon-refresh"
							name="apply_coupon"
							value="<?php esc_attr_e( 'Apply coupon', 'woocommerce' ); ?>">
						<?php esc_attr_e( 'Aktualizuj koszyk', 'woocommerce' ); ?>

						<button type="submit" class="button" name="update_cart"
								value="<?php esc_attr_e( 'Update cart', 'woocommerce' ); ?>"><?php esc_html_e( 'Update cart', 'woocommerce' ); ?></button>
					</button>
					<?php do_action( 'woocommerce_cart_coupon' ); ?>
				</div>
			<?php } ?>

			<?php do_action( 'woocommerce_cart_actions' ); ?>

			<?php wp_nonce_field( 'woocommerce-cart', 'woocommerce-cart-nonce' ); ?>
			<?php do_action( 'woocommerce_after_cart_table' ); ?>
		</form>
	</div>

	<?php do_action( 'woocommerce_before_cart_collaterals' ); ?>

	<div class="col-4" style="background-color:#942443;">
		<?php
		/**
		 * Cart collaterals hook.
		 *
		 * @hooked woocommerce_cross_sell_display
		 * @hooked woocommerce_cart_totals - 10
		 */
		do_action( 'woocommerce_cart_collaterals' );
		?>
	</div>
</div>
<?php do_action( 'woocommerce_after_cart' ); ?>
