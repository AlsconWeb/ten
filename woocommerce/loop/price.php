<?php
/**
 * Loop Price
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/loop/price.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @package     WooCommerce\Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product, $woocommerce;
?>

<div class="price-block">
	<?php if ( $product->is_type( 'simple' ) ) : ?>
		<p class="price">
			<?php if ( $product->get_sale_price() ): ?>
				<?php echo esc_html( $product->get_sale_price() . ' ' . get_woocommerce_currency_symbol() ); ?>
				<span><?php echo esc_html( $product->get_regular_price() . ' ' . get_woocommerce_currency_symbol() ); ?></span>
			<?php else: ?>
				<?php echo esc_html( $product->get_regular_price() . ' ' . get_woocommerce_currency_symbol() ); ?>
			<?php endif; ?>
		</p>
	<?php endif; ?>
	<?php if ( $product->is_type( 'variable' ) ) : ?>
		<?php
		$min = $product->get_variation_price( 'min', true );
		$max = $product->get_variation_price( 'max', true );
		?>
		<p class="price">
			<?php echo esc_html( $min . ' ' . get_woocommerce_currency_symbol() ); ?>
			<?php if ( $min !== $max ) : ?>
				<span class="max"><?php echo esc_html( $max . ' ' . get_woocommerce_currency_symbol() ); ?></span>
			<?php endif; ?>
		</p>
	<?php endif; ?>
</div>
