<?php
/**
 * Content wrappers
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/global/wrapper-start.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @package     WooCommerce\Templates
 * @version     3.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>
	<section class="products">
	<div class="container-fluid">
	<div class="row align-items-center">
<?php
$current_term_obg = get_queried_object();

if ( isset( $current_term_obg->term_id ) ) {
	$influencer = carbon_get_term_meta( $current_term_obg->term_id, 'ten_association_influences' );
	if ( isset( $influencer[0]['id'] ) ) {
		$image = get_the_post_thumbnail_url( $influencer[0]['id'], 'full' );
	}
}
?>
<?php if ( ! empty( $influencer ) ) : ?>
	<div class="col col-auto">
		<div class="left-block">
			<a class="icon-x" href="<?php echo esc_url( get_the_permalink( $influencer[0]['id'] ) ); ?>"></a>
			<img
					src="<?php echo esc_url( $image ); ?>"
					alt="<?php echo esc_html( get_the_title( $influencer[0]['id'] ) ); ?>">
		</div>
	</div>
<?php endif; ?>
<?php if ( ! is_singular( 'product' ) ) : ?>
	<div class="col">
<?php endif; ?>