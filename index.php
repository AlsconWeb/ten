<?php get_header();
global $post;
?>
	<section>
		<div class="container-fluid">
			<?php if ( have_posts() ) : ?>
				<div class="row">
					<?php
					while ( have_posts() ) :
						the_post();
						?>
						<?php the_content(); ?>
					<?php endwhile; ?>
				</div>
			<?php endif; ?>
		</div>
	</section>
<?php get_footer();

