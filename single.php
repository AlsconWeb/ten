<?php
/**
 * Influenced biography page.
 *
 * @package TEN
 */

get_header();
?>
	<section>
		<div class="container-fluid">
			<div class="row">
				<?php if ( have_posts() ) : ?>
					<?php
					while ( have_posts() ) :
						the_post();
						$influencer_id       = get_the_ID();
						$id_product_category = carbon_get_post_meta( $influencer_id, 'ten_association_product' );
						$color               = carbon_get_term_meta( (int) $id_product_category[0]['id'], 'influencer_color' );
						?>
						<div class="bio">
							<div class="item">
								<div
										class="img"
										style="color:<?php echo esc_attr( ! empty( $color ) ? $color : '#FFF' ); ?>;"
										data-title="<?php the_title(); ?>">
									<?php if ( has_post_thumbnail( $influencer_id ) ) : ?>
										<?php the_post_thumbnail( 'full' ); ?>
									<?php else : ?>
										<img src="https://via.placeholder.com/570x665.png?text=No+Image" alt="No Image">
									<?php endif ?>
								</div>
								<div class="description">
									<div class="arrows">
										<?php do_action( 'get_pagination_post' ); ?>
									</div>
									<h1 class="title"><?php the_title(); ?></h1>
									<div class="excerpt">
										<?php the_content(); ?>
									</div>
									<a
											class="button icon-chevron"
											href="<?php echo esc_url( get_term_link( (int) $id_product_category[0]['id'], $id_product_category[0]['subtype'] ) ); ?>">
										<?php esc_html_e( 'Zobacz kolekcję', 'ten' ); ?>
									</a>
								</div>
							</div>
						</div>
					<?php endwhile; ?>
				<?php endif; ?>
			</div>
		</div>
	</section>
<?php
get_footer();
