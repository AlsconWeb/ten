<?php
/**
 * Template 404 Error.
 *
 * @package TEN
 */

get_header();
?>
	<section>
		<div class="container-fluid">
			<div class="row align-items-center">
				<div class="col-8">
					<h1>
						<i class="icon-four"></i>
						<i class="icon-x"></i>
						<i class="icon-four"></i>
					</h1>
				</div>
				<div class="col-4">
					<div class="description">
						<h2 class="title"><?php esc_html_e( 'Przepraszamy ta strona nie istnieje', 'ten' ); ?></h2>
						<?php echo wp_kses_post( wpautop( carbon_get_theme_option( 'ten_404_text' ) ) ); ?>
						<a
								class="icon-left"
								href="<?php echo esc_url( get_permalink( wc_get_page_id( 'shop' ) ) ); ?>">
							<?php esc_attr_e( 'Powrót do strony głównej sklepu', 'ten' ); ?>
						</a>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php
get_footer();
