<?php
/**
 * Serach form template.
 *
 * @package TEN
 */
?>
<form
		role="search"
		method="get" id="searchform"
		action="<?php echo esc_url( home_url( '/' ) ); ?>">
	<div class="input">
		<input
				type="text"
				value="<?php echo get_search_query(); ?>"
				placeholder="<?php esc_attr_e( 'Czego szukasz?', 'ten' ); ?>"
				name="s"
				id="s">
	</div>
</form>
