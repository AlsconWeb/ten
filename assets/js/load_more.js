/* global tenData, jQuery */
jQuery( document ).ready( function( $ ) {
	let load_more = $( '#ajax-loade-more' );

	if ( load_more.length > 0 ) {
		$( load_more ).click( function( e ) {
			e.preventDefault();
			let maxPage = $( this ).data( 'max_page' )

			let data = {
				action: 'lode_more',
				query: $( this ).data( 'query' ),
				nonce: tenData.nonce,
				page: $( this ).data( 'current_page' )
			}

			$.ajax( {
				type: 'POST',
				url: tenData.url,
				data: data,
				success: function( response ) {
					if ( response.success ) {
						$( '.products-slider' ).slick( 'slickAdd', response.data.html ).slick( 'slickNext' );
						load_more.data( 'current_page', response.data.paged )
						if ( response.data.paged >= maxPage ) {
							load_more.hide()
						}
					}
				},
				error: function( xhr, ajaxOptions, thrownError ) {
					console.log( 'error...', xhr );
					//error logging
				},
				complete: function() {
					$( '.choice .select > select' ).each( function( i, el ) {
						$( el ).select2( {
							minimumResultsForSearch: Infinity
						} );
					} )


				}
			} );

		} );
	}
} );