jQuery( document ).ready( function( $ ) {
	if ( $( '#language' ).length ) {
		$( '#language' ).change( function( e ) {
			if ( $( this ).prop( 'checked' ) ) {
				$( '.language' ).data( 'active_lang', 'en' );
				location.href = $( '.language' ).data( 'url_en' )
			} else {
				$( '.language' ).data( 'active_lang', 'pl' );
				location.href = $( '.language' ).data( 'url_pl' )
			}
		} )
	}
	var pattern = /(Instagram)/,
		str = navigator.userAgent;
	if ( pattern.test( str ) ) {
		var windowHeight = $( 'html' ).height() + 60
		$( 'body:not(.woocommerce-checkout)' ).css( 'height', windowHeight + 'px' );
	}

	let next = $( '#next-slide' );
	if ( next.length ) {
		next.click( function( e ) {
			e.preventDefault();
			let slider = $( '.products-slider' );
			let currentSlide = slider.slick( 'slickCurrentSlide' );
			let allSlide = $( '.slick-dots li' ).length * 3;
			if ( allSlide === currentSlide + 3 ) {
				slider.slick( 'slickGoTo', 1 )
			} else {
				slider.slick( 'slickNext' )
			}
		} )
	}
     if ($('.products-slider').length) {
        $('.products-slider img').each(function () {
            var imgHeight = $(this).innerHeight() / 2;
            setTimeout(function () {
                $('.products-slider .slick-arrow').css('top', imgHeight + 'px');
            }, 500)
        });
    }
    if ($('.woocommerce-checkout').length) {
        if (/apple/i.test(navigator.vendor)) {
            $('html').addClass('ios-html');
        }
    }
	if ( $( window ).height() < 440 ) {
		if ( $( window ).width() < 992 ) {
			$( '.products-slider .title' ).each( function() {
				var titleSlider = $( this )
				$( this ).parent().find( '.choice' ).prepend( titleSlider )
			} )
		}
	}
	
	$( window ).on( 'resize', function() {
        $('.products-slider img').each(function () {
            var imgHeight = $(this).innerHeight() / 2;
            $('.products-slider .slick-arrow').css('top', imgHeight + 'px');
        });
		if ( $( window ).height() < 440 ) {
			if ( $( window ).width() < 992 ) {
				$( '.products-slider .title' ).each( function() {
					var titleSlider = $( this )
					$( this ).parent().find( '.choice' ).prepend( titleSlider )
				} )
			}
		}
	} );

	setTimeout( function() {
		$( '.button[name=update_cart]' ).attr( 'disabled', false );
	}, 400 );

	$( '.input-text.qty' ).change( function() {
		$( '.button[name=update_cart]' ).trigger( 'click' );
	} )

	$( '.increment, .decrement' ).click( function() {
		setTimeout( function() {
			$( '.button[name=update_cart]' ).trigger( 'click' );
		}, 1000 )
	} )

	$( '.cart-discount .woocommerce-remove-coupon' ).click( function() {
		$( '.cart-discount' ).remove();
	} )
} );