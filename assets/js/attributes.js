/* global tenColorProduct */
jQuery( document ).ready( function( $ ) {
	let sizesEL = $( '.select select[name=sizes]' );
	let sizeFlag = false
	let colorFlag = false;
	let quantityValue = 1;
	let currentSize = '';
	let currentColor = '';

	onClickToColor();

	defaultProductAttribute();

	defaultProductAttributeInCatalogPage();

	sizesEL.change( function( e ) {
		let singlePageSelectSize = $( '#pa_sizes' )
		let container = $( this ).parents( '.choice' ).find( 'a.button' );
		let prodID = $( this ).parent().parent().parent().find( '.radio-items' ).data( 'prod_id' );
		let productColor = $( this ).parent().parent().parent().find( '.radio-items' );
		let all_attributes = $( this ).parent().parent().parent().find( '.radio-items' ).data( 'attributes' );
		let size = $( this ).val();
		let colors = get_all_color_in_sizes( size, all_attributes, $( this ) );
		currentSize = size;


		sizeFlag = true;
		productColor.find( '.radio' ).remove();
		$.each( colors, function( i, el ) {
			let html = `
				<div class="radio">
				<input type="radio" id="${el}-${tenColorProduct.all_colors[ el ][ 'term_id' ]}-${prodID}" name="color-${prodID}" value="${el}">
				<label for="${el}-${tenColorProduct.all_colors[ el ][ 'term_id' ]}-${prodID}" style="color:${tenColorProduct.all_colors[ el ][ 'color' ]};"></label>
				</div>
				`;
			productColor.append( html );
		} );
		onClickToColor()

		if ( currentColor.length && currentColor ) {
			$( `#${currentColor}` ).prop( 'checked', true )
		}

		singlePageSelectSize.val( size )
		get_cart_url( container, colorFlag, sizeFlag );

	} );

	/**
	 * Click on color
	 */
	function onClickToColor() {
		let colorsEl = $( '.radio input[type=radio]' );

		colorsEl.click( function( e ) {
			let singlePageSelectColor = $( '#pa_color' );
			let imageID = $( this ).data( 'image-id' );
			let container = $( this ).parents( '.choice' ).find( 'a.button' );
			let all_attributes = $( this ).parent().parent().parent().find( '.radio-items' ).data( 'attributes' );
			let sizes = get_all_sizes_in_color( $( this ).val(), all_attributes )
			let sizes_select = $( this ).parent().parent().parent().find( '.select select' )
			colorFlag = true;
			currentColor = $( this ).attr( 'id' );

			if ( ! sizeFlag ) {
				sizes_select.find( 'option' ).remove()
				sizes_select.append( `<option value="0">${tenColorProduct.text_size_select}</option>` );
				$.each( sizes, function( i, el ) {
					sizes_select.append( `<option value="${el}">${el}</option>` );
				} );
			}
			singlePageSelectColor.val( $( this ).val() )
			get_cart_url( container, colorFlag, sizeFlag );

			change_image_slide( imageID );
		} );
	}

	/**
	 * Get All Colors in Sizes.
	 *
	 * @param size Size
	 * @param all_attributes All Attributes.
	 * @param element Element.
	 * @returns {*[]} Array Colors.
	 */
	function get_all_color_in_sizes( size, all_attributes, element ) {
		let colors = [];
		if ( size !== '0' ) {
			$.each( all_attributes, function( i, el ) {
				if ( el.attributes.attribute_pa_sizes === size ) {
					colors.push( el.attributes.attribute_pa_color );
				}
			} );
		}

		if ( size === '0' ) {
			let defaultColor = $( element ).parent().parent().find( '.radio-items' ).data( 'default-color' )
			$.each( defaultColor, function( i, el ) {
				colors.push( el );
			} );
		}

		return colors;
	}


	/**
	 * Get All Sizes in Color.
	 *
	 * @param color Color all.
	 * @param all_attributes All Attributes.
	 * @returns {*[]} Array sizes.
	 */
	function get_all_sizes_in_color( color, all_attributes ) {
		let sizes = [];

		$.each( all_attributes, function( i, el ) {

			if ( el.attributes.attribute_pa_color === color ) {
				sizes.push( el.attributes.attribute_pa_sizes );
			}
		} );

		return sizes;
	}

	/**
	 * Generate Url add to cart.
	 *
	 * @param container Container.
	 * @param colorFlag Color Flag.
	 * @param sizeFlag Sizes Flag.
	 */
	function get_cart_url( container, colorFlag, sizeFlag ) {
		if ( colorFlag && sizeFlag ) {
			let url = $( container ).attr( 'href' );
			let all_attributes = $( container ).parents( '.choice' ).find( '.radio-items' ).data( 'attributes' );
			let color = $( container ).parents( '.choice' ).find( '.radio-items input[type=radio]:checked' ).val()
			let size = $( container ).parents( '.choice' ).find( '.select select' ).val()
			$.each( all_attributes, function( index, value ) {
				if ( value.attributes.attribute_pa_color === color && value.attributes.attribute_pa_sizes === size ) {
					url = '?add-to-cart=' + value.variation_id + '&quantity=' + quantityValue
					$( container ).attr( 'href', url ).attr( 'data-product_id', value.variation_id )
					$( container ).removeClass( 'disable' );
				}
			} );
		}
	}

	/**
	 * Delete get params after add to cart.
	 *
	 * @type {string}
	 */
	let query = window.location.search.substring( 1 )

	// is there anything there ?
	if ( query.length ) {
		// are the new history methods available ?
		if ( window.history != undefined && window.history.pushState != undefined ) {
			// if pushstate exists, add a new state to the history, this changes the url without reloading the page
			window.history.pushState( {}, document.title, window.location.pathname );
			window.localStorage.setItem( 'currentSlide', $( '.archive .products-slider' ).slick( 'slickCurrentSlide' ) );
		}
	}

	/**
	 * Change quantity
	 *
	 * @type {jQuery|HTMLElement|*}
	 */
	let quantity = $( '.quantity' );

	if ( quantity.length ) {
		$( quantity ).change( function( e ) {
			let value = $( this ).val()
			quantityValue = value
			get_cart_url( $( '.button.icon-plus' ), colorFlag, sizeFlag )
			$( '.input-text.qty' ).val( value );
		} );
	}

	let total = parseInt( $( '.icon-cart span' ).text() );
	$( '.ajax_add_to_cart' ).click( function() {
		if ( ! $( '.icon-cart span' ).text().length ) {
			total = 0
		}
		$( this ).removeClass( 'icon-plus' )
		total++;
		$( '.icon-cart span' ).text( total ); //update
		jQuery( 'body' ).trigger( 'wc_fragments_refreshed' );
		$( document.body ).trigger( 'wc_fragment_refresh' );
	} );
	jQuery( 'body' ).trigger( 'wc_fragments_refreshed' );
	$( document.body ).trigger( 'wc_fragment_refresh' );

	/**
	 * Default product attribute in product page.
	 */
	function defaultProductAttribute() {
		let valueColor = $( '#pa_color' ).val();
		let valueSize = $( '#pa_sizes' ).val();

		let color = $( '.radio input[type=radio]' );
		let sizes = $( '.select select[name=sizes]>option' );

		let container = $( '.choice' ).find( 'a.button' );

		$( color ).each( function( index, item ) {
			if ( $( this ).val() === valueColor ) {
				$( this ).prop( 'checked', true );
			}
		} );

		$( sizes ).each( function( index, value ) {
			if ( $( this ).val() === valueSize ) {
				$( this ).attr( 'selected', 'selected' );
				sizes.trigger( 'change' );
			}
		} );

		get_cart_url( container, true, true );
	}

	function defaultProductAttributeInCatalogPage() {
		let El = $( '.item .choice' );
		if ( El.length ) {
			$( El ).each( function( index, item ) {
				let defaultAttr = $( this ).find( '.radio-items' ).data( 'default-attributes' );

				if ( ! defaultAttr ) {
					return;
				}

				let size = $( this ).find( '.select select[name=sizes]>option' );
				let color = $( this ).find( '.radio input[type=radio]' );
				let container = $( this ).find( '.ajax_add_to_cart' );

				$( size ).each( function( index, value ) {
					if ( $( value ).val() === defaultAttr.pa_sizes ) {
						$( value ).attr( 'selected', 'selected' );
						size.trigger( 'change' );
					}
				} );

				$( color ).each( function( index, item ) {
					if ( $( this ).val() === defaultAttr.pa_color ) {
						$( this ).prop( 'checked', true );
					}
				} );

				get_cart_url( container, true, true );
			} );
		}
	}


	/**
	 * Set Url if not emprty cookie url.
	 *
	 * @type {jQuery|HTMLElement|*}
	 */
	let backURL = $( '.products .icon-left' );
	if ( backURL.length && location.href !== getCookie( 'back_to_url' ) ) {
		console.log( getCookie( 'back_to_url' ) )
		backURL.attr( 'href', getCookie( 'back_to_url' ) );
	}

	/**
	 * Get Cookie.
	 *
	 * @param cname cookie name.
	 * @returns {string} cookie value.
	 */
	function getCookie( cname ) {
		let name = cname + '=';
		let decodedCookie = decodeURIComponent( document.cookie );
		let ca = decodedCookie.split( ';' );
		for ( let i = 0; i < ca.length; i++ ) {
			let c = ca[ i ];
			while ( c.charAt( 0 ) == ' ' ) {
				c = c.substring( 1 );
			}
			if ( c.indexOf( name ) == 0 ) {
				return c.substring( name.length, c.length );
			}
		}
		return '';
	}

	/**
	 * Change slide to variation color.
	 *
	 * @param imageID
	 */
	function change_image_slide( imageID ) {
		let slider = $( '.product-slider' );
		let slide_index = $( `img[data-vaiastion-image-id=${imageID}]` ).parents( '.item' ).data( 'slick-index' );

		slider.slick( 'slickGoTo', slide_index );
	}
} );