<?php
/**
 * Hader template.
 *
 * @package TEN
 */

$google_font_url = carbon_get_theme_option( 'ten_google_front' );
$logo            = carbon_get_theme_option( 'ten_logo' );
$facebook        = carbon_get_theme_option( 'ten_facebook_link' );
$twitter         = carbon_get_theme_option( 'ten_twitter_link' );
$instagram       = carbon_get_theme_option( 'ten_instagram_link' )

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" id="viewport"
		  content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no"/>
	<title><?php bloginfo( 'name' ); ?> | <?php bloginfo( 'desription' ); ?></title>
	<link
			rel="apple-touch-icon"
			sizes="57x57"
			href="<?php echo esc_url( TEN_THEME_URL . '/assets/img/favicon/apple-icon-57x57.png' ); ?>">
	<link
			rel="apple-touch-icon"
			sizes="60x60"
			href="<?php echo esc_url( TEN_THEME_URL . '/assets/img/favicon/apple-icon-60x60.png' ); ?>">
	<link
			rel="apple-touch-icon"
			sizes="72x72"
			href="<?php echo esc_url( TEN_THEME_URL . '/assets/img/favicon/apple-icon-72x72.png' ); ?>">
	<link
			rel="apple-touch-icon"
			sizes="76x76"
			href="<?php echo esc_url( TEN_THEME_URL . '/assets/img/favicon/apple-icon-76x76' ); ?>">
	<link
			rel="apple-touch-icon"
			sizes="114x114"
			href="<?php echo esc_url( TEN_THEME_URL . '/assets/img/favicon/apple-icon-114x114.png' ); ?>">
	<link
			rel="apple-touch-icon"
			sizes="120x120"
			href="<?php echo esc_url( TEN_THEME_URL . '/assets/img/favicon/apple-icon-120x120.png' ); ?>">
	<link
			rel="apple-touch-icon"
			sizes="144x144"
			href="<?php echo esc_url( TEN_THEME_URL . '/assets/img/favicon/apple-icon-144x144.png' ); ?>">
	<link
			rel="apple-touch-icon"
			sizes="152x152"
			href="<?php echo esc_url( TEN_THEME_URL . '/assets/img/favicon/apple-icon-152x152.png' ); ?>">
	<link
			rel="apple-touch-icon"
			sizes="180x180"
			href="<?php echo esc_url( TEN_THEME_URL . '/assets/img/favicon/apple-icon-180x180.png' ); ?>">
	<link
			rel="icon"
			type="image/png"
			sizes="192x192"
			href="<?php echo esc_url( TEN_THEME_URL . '/assets/img/favicon/android-icon-192x192.png' ); ?>">
	<link
			rel="icon"
			type="image/png"
			sizes="32x32"
			href="<?php echo esc_url( TEN_THEME_URL . '/assets/img/favicon/favicon-32x32.png' ); ?>">
	<link
			rel="icon"
			type="image/png"
			sizes="96x96"
			href="<?php echo esc_url( TEN_THEME_URL . '/assets/img/favicon/favicon-96x96.png' ); ?>">
	<link
			rel="icon"
			type="image/png"
			sizes="16x16"
			href="<?php echo esc_url( TEN_THEME_URL . '/assets/img/favicon/favicon-16x16.png' ); ?>">
	<link
			rel="manifest"
			href="<?php echo esc_url( TEN_THEME_URL . '/assets/img/favicon/manifest.json' ); ?>">
	<meta name="msapplication-TileColor" content="transparent">
	<meta
			name="msapplication-TileImage"
			content="<?php echo esc_url( TEN_THEME_URL . '/assets/img/favicon/ms-icon-144x144.png' ); ?>">
	<meta name="theme-color" content="transparent">
	<link href="<?php echo esc_url( $google_font_url ); ?>" rel="stylesheet">

	<?php wp_head(); ?>
</head>
<?php
$items_count = WC()->cart->get_cart_contents_count();
?>
<body <?php body_class(); ?>>
<header class="<?php echo empty( $items_count ) ? 'cart-empty-class' : ''; ?>">
	<div class="container-fluid">
		<div class="row">
			<div class="col">
				<a class="logo" href="<?php bloginfo( 'url' ); ?>">
					<?php if ( ! empty( $logo ) ) : ?>
						<img src="<?php echo esc_url( $logo ); ?>" alt="logo">
					<?php else : ?>
						<h3><?php bloginfo( 'name' ); ?></h3>
					<?php endif; ?>
				</a>
			</div>
			<div class="col-auto">
			</div>
			<div class="col-auto">
				<i class="icon-cart"><span><?php echo $items_count ? esc_attr( $items_count ) : ''; ?></span></i>
				<div class="cart">
					<i class="icon-close"></i>
					<div class="mini-cart-wrapper">
						<h2 class="title">
							<?php
							echo wp_kses_post(
								sprintf(
								/* translators: %d: count product in cart */
									__( 'Twój koszyk zawiera <span>%d </span>produkty', 'tent' ),
									$items_count
								)
							);
							?>
						</h2>
						<div class="mini-cart-items-wrapper"></div>
					</div>
				</div>
			</div>
			<div class="col-auto">
				<?php
				do_action( 'custom_switcher' );
				?>
			</div>
			<div class="col-auto">
				<div class="burger-menu"><span></span><span></span><span></span><span></span><span></span></div>
				<div class="menu-block">
					<i class="icon-close"></i>
					<?php
					wp_nav_menu(
						[
							'theme_location' => 'header_menu',
							'menu'           => 'Menu in Header',
							'container'      => '',
							'menu_class'     => 'menu',
						]
					);
					?>
					<ul class="soc">
						<?php if ( ! empty( $facebook ) ) : ?>
							<li class="icon-facebook">
								<a href="<?php echo esc_url( $facebook ); ?>"></a>
							</li>
						<?php endif; ?>
						<?php if ( ! empty( $instagram ) ) : ?>
							<li class="icon-instagram">
								<a href="<?php echo esc_url( $instagram ); ?>"></a>
							</li>
						<?php endif; ?>
						<?php if ( ! empty( $twitter ) ) : ?>
							<li class="icon-twitter">
								<a href="<?php echo esc_url( $twitter ); ?>"></a>
							</li>
						<?php endif; ?>
					</ul>
					<?php
					wp_nav_menu(
						[
							'theme_location' => 'header_menu_button',
							'menu'           => 'Header Button menu',
							'container'      => '',
							'menu_class'     => 'sub-menu',
						]
					);
					?>
				</div>
			</div>
		</div>
	</div>
</header>
