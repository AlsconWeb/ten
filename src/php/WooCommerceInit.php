<?php
/**
 * Init Woocommerce support Theme.
 *
 * @package TEN
 */

namespace TEN;

use TEN\WooCommerce\Cart\ChangeCart;
use TEN\WooCommerce\Cart\MiniCart;
use TEN\WooCommerce\Product\ProductMetaInfluences;
use TEN\WooCommerce\Product\SalesBadge;
use TEN\WooCommerce\ProductArchive\ChangeCategory;
use TEN\WooCommerce\ProductArchive\LoadMoreProduct;

/**
 * WooCommerceInit class file.
 */
class WooCommerceInit {

	/**
	 * The number of products displayed on one page.
	 */
	protected const COUNT_OUTPUT_PRODUCT = - 1;

	/**
	 * Custom Checkout fields.
	 *
	 * @var array[]
	 */
	private array $ten_checkout_fields;

	/**
	 * WooCommerceInit construct.
	 */
	public function __construct() {
		$this->hook_init();
		$this->remove_action();
		$this->set_cookie();
		new ChangeCategory();
		new SalesBadge();
		new LoadMoreProduct();
		new ProductMetaInfluences();

		$this->ten_checkout_fields = [
			[
				'name' => 'terms_and_conditions',
				'arg'  => [
					'type'     => 'checkbox',
					'id'       => 'terms_and_conditions',
					'required' => true,
					'class'    => [ 'input-checkbox' ],
					'label'    => sprintf(
						'%s <a href="%s">%s</a> %s <a href="%s">%s</a> %s <a href="%s">%s</a>',
						__( 'Zapoznałem/am się i akceptuję', 'ten' ),
						'https://tenfajnysklep.pl/wp-content/uploads/2022/05/Regulamin_tenfajnysklep-1.pdf',
						__( 'regulamin', 'ten' ),
						__( 'i', 'ten' ),
						'https://tenfajnysklep.pl/wp-content/uploads/2022/05/POLITYKA-PRYWATNOS%CC%81CI-TFS.pdf',
						__( 'politykę prywatności', 'ten' ),
						__( 'sklepu internetowego', 'ten' ),
						get_bloginfo( 'url' ),
						'tenfajnysklep.pl'
					),
				],
			],
			[
				'name' => 'personal_data',
				'arg'  => [
					'type'     => 'checkbox',
					'class'    => [ 'input-checkbox' ],
					'required' => true,
					'label'    => __( 'Wyrażam zgodę na przetworzenie moim danych osobowych przez Creative Factory Sp. z o.o. na potrzeby realizacji zamówienia.', 'north' ),
				],
			],
			[
				'name' => 'marketing_opting',
				'arg'  => [
					'type'  => 'checkbox',
					'class' => [ 'input-checkbox' ],
					'label' => __( 'Wyrażam zgodę na otrzymywanie newslettera z informacjami o promocjach i nowościach.', 'north' ),
				],
			],
		];
	}

	/**
	 * Add and remove Hooks WooCommerce.
	 *
	 * @return void
	 */
	private function hook_init(): void {
		add_filter( 'woocommerce_add_to_cart_fragments', [ $this, 'refresh_cart_count' ] );

		add_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_link_close', 20 );
		add_action( 'woocommerce_shop_loop_item_title', [ $this, 'show_title_in_products_loop' ], 10 );
		add_action(
			'show_variation_attributes_in_product_loop',
			[
				$this,
				'show_variation_attributes_in_product_loop',
			],
			10
		);
		add_action( 'woocommerce_before_single_product_summary', [ $this, 'show_product_head' ], 10 );
		add_action( 'woocommerce_before_single_product_summary', 'woocommerce_template_single_price', 15 );
		add_action( 'single_product_slider', [ $this, 'show_single_slider' ], 10 );
		add_filter( 'loop_shop_per_page', [ $this, 'get_count_output_product' ], 20 );
		add_action( 'wp_enqueue_scripts', [ $this, 'add_scripts' ] );
		add_action( 'woocommerce_checkout_before_terms_and_conditions', [ $this, 'add_terms_checkbox' ] );
		add_action( 'woocommerce_checkout_update_order_meta', [ $this, 'save_data' ] );
		add_action( 'woocommerce_admin_order_data_after_billing_address', [ $this, 'output_in_order' ] );
		add_action( 'show_category', 'woocommerce_output_all_notices', 30 );
		add_action( 'get_next_prev_post', [ $this, 'show_next_prev_product' ] );

		add_action( 'current_screen', [ $this, 'fix_wpml_error' ], 20, 1 );

		new ChangeCart();
	}

	/**
	 * Update Mini Cart.
	 *
	 * @param array $fragments Fragments.
	 *
	 * @return array
	 */
	public function refresh_cart_count( array $fragments ): array {
		ob_start();
		$items_count = WC()->cart->get_cart_contents_count();
		$mini_cart   = new MiniCart();
		?>
		<i class="icon-cart"><span><?php echo $items_count ? esc_attr( $items_count ) : ''; ?></span></i>
		<?php
		$fragments['.icon-car'] = ob_get_clean();
		ob_start();
		echo $mini_cart->show_mini_cart();
		$fragments['div.mini-cart-wrapper'] = ob_get_clean();

		return $fragments;
	}

	/**
	 * Remove WooCommerce Action.
	 *
	 * @return void
	 */
	private function remove_action(): void {
		remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20 );
		remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_product_link_close', 5 );
		remove_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_show_product_loop_sale_flash', 10 );
		remove_action( 'woocommerce_shop_loop_item_title', 'woocommerce_template_loop_product_title', 10 );
		remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 5 );
		remove_action( 'woocommerce_before_single_product_summary', 'woocommerce_show_product_sale_flash', 10 );
		remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 );
		remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10 );
		remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_sharing', 50 );
		remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_title', 5 );
		remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );
	}

	/**
	 * Show the product title in the product loop.
	 *
	 * @return void
	 */
	public function show_title_in_products_loop(): void {
		echo sprintf(
			'<h3 class="title"><a href="%s">%s</a></h2>',
			esc_url( get_the_permalink() ),
			esc_html( get_the_title() )
		);
	}

	/**
	 * Show Variation Attributes colors and sizes
	 *
	 * @return void
	 */
	public function show_variation_attributes_in_product_loop(): void {
		$available_variation = $this->get_variation_attributes();
		$default_color       = $this->get_default_product_color();
		ob_start();
		include __DIR__ . '/WooCommerce/template/select_sizes.php';
		include __DIR__ . '/WooCommerce/template/select_color.php';
		if ( is_singular( 'product' ) ) {
			include __DIR__ . '/WooCommerce/template/input_quantity.php';
		}
		// phpcs:disable
		echo ob_get_clean();
		//phpcs:enable

	}

	/**
	 * Get variation attributes.
	 *
	 * @return array
	 */
	public function get_variation_attributes(): array {
		global $product;

		if ( $product->is_type( 'variable' ) ) {
			$available_variations = $product->get_available_variations();
			$variation            = [];
			$all_attributes       = [];
			foreach ( $available_variations as $key => $value ) {
				$variation['all_attributes'][ $key ]['attributes']   = $value['attributes'];
				$variation['all_attributes'][ $key ]['variation_id'] = $value['variation_id'];

				foreach ( $value['attributes'] as $name => $attribute ) {
					$all_attributes[ $name ][] = $attribute;
				}
			}

			if ( function_exists( 'array_unique_recursive' ) ) {
				$variation['all'] = array_unique_recursive( $all_attributes );
			}

		}

		if ( empty( $variation ) ) {
			return [];
		}

		return $variation;
	}

	/**
	 * Get default colors in variation product.
	 *
	 * @return array
	 */
	public function get_default_product_color(): array {
		global $product;

		if ( $product->is_type( 'variable' ) ) {
			$available_variations = $product->get_available_variations();
			$all_colors           = [];
			foreach ( $available_variations as $value ) {
				if ( isset( $value['attributes']['attribute_pa_color'] ) ) {
					$all_colors[] = $value['attributes']['attribute_pa_color'];
				} else {
					break;
				}
			}
			if ( function_exists( 'array_unique_recursive' ) ) {
				return array_unique_recursive( $all_colors );
			}

			return $all_colors;

		}

		return [];
	}

	/**
	 * Add Scripts and Styles in wc templates.
	 *
	 * @return void
	 */
	public function add_scripts(): void {
		if ( is_woocommerce() ) {
			wp_enqueue_script( 'color', TEN_THEME_URL . '/assets/js/attributes.min.js', [ 'jquery' ], TEN_THEME_VERSTION, true );
			wp_localize_script(
				'color',
				'tenColorProduct',
				[
					'all_colors'       => $this->get_all_product_colors(),
					'text_size_select' => __( 'Rozmiary', 'ten' ),
				]
			);
		}

		if ( is_archive() ) {
			wp_enqueue_script(
				'load_more',
				TEN_THEME_URL . '/assets/js/load_more.min.js',
				[
					'jquery',
					'build',
				],
				TEN_THEME_VERSTION,
				true
			);
			wp_localize_script(
				'load_more',
				'tenData',
				[
					'url'   => admin_url( 'admin-ajax.php' ),
					'nonce' => wp_create_nonce( 'load-more-nonce' ),
				]
			);
		}
	}

	/**
	 * Get All colors and color code.
	 *
	 * @return array
	 */
	public function get_all_product_colors(): array {
		$colors_term = get_terms(
			[
				'taxonomy'   => 'pa_color',
				'hide_empty' => false,
			]
		);

		if ( empty( $colors_term ) || is_wp_error( $colors_term ) ) {
			return [];
		}

		$colors = [];
		foreach ( $colors_term as $color_term ) {
			$colors[ $color_term->slug ] = [
				'color'   => carbon_get_term_meta( $color_term->term_id, 'product_color' ),
				'term_id' => $color_term->term_id,
			];
		}

		return $colors;
	}

	/**
	 * Filter number of products displayed on one page.
	 *
	 * @return int
	 */
	public function get_count_output_product(): int {

		return self::COUNT_OUTPUT_PRODUCT;
	}

	/**
	 * Output product header.
	 *
	 * @return void
	 */
	public function show_product_head(): void {
		global $product;
		$product_id    = $product->get_id();
		$propduct_meta = new ProductMetaInfluences();
		$meta          = $propduct_meta->get_product_meta();
		$referer       = get_permalink( wc_get_page_id( 'shop' ) );
		if ( ! empty( $meta['url'] ) ) {
			$referer = $meta['url'];
		}

		?>
		<a class="icon-left" href="<?php echo esc_url( $referer ); ?>">
			<?php esc_attr_e( 'Wróć do kolekcji', 'ten' ); ?>
		</a>
		<?php
		$propduct_meta = new ProductMetaInfluences();
		$propduct_meta->show_product_meta();
		?>
		<h1 class="title"><?php echo esc_html( get_the_title( $product_id ) ); ?></h1>
		<?php
	}

	/**
	 * Output template product-slider.
	 *
	 * @return void
	 */
	public function show_single_slider(): void {
		wc_get_template( 'single-product/product-slider.php' );
	}

	/**
	 * Add terms checkbox.
	 */
	public function add_terms_checkbox(): void {
		global $woocommerce;

		foreach ( $this->ten_checkout_fields as $field ) {
			echo "<div id='" . esc_attr( $field['name'] ) . "'>";
			woocommerce_form_field(
				$field['name'],
				$field['arg'],
				$woocommerce->checkout->get_value( $field['name'] )
			);
			echo '</div>';
		}

	}

	/**
	 * Save Custom data.
	 *
	 * @param int $order_id Order Id.
	 */
	public function save_data( int $order_id ): void {
		//phpcs:disable
		$terms_and_conditions = isset( $_POST['terms_and_conditions'] ) ? filter_var( wp_unslash( $_POST['terms_and_conditions'] ), FILTER_SANITIZE_NUMBER_INT ) : false;
		$personal_data        = isset( $_POST['personal_data'] ) ? filter_var( wp_unslash( $_POST['personal_data'] ), FILTER_SANITIZE_NUMBER_INT ) : false;
		$marketing_opting     = isset( $_POST['marketing_opting'] ) ? filter_var( wp_unslash( $_POST['marketing_opting'] ), FILTER_SANITIZE_NUMBER_INT ) : false;
		//phpcs:enable

		if ( $terms_and_conditions ) {
			update_post_meta( $order_id, 'iwp_terms', $terms_and_conditions );
		}

		if ( $personal_data ) {
			update_post_meta( $order_id, 'iwp_personal_data', $personal_data );
		}

		if ( $marketing_opting ) {
			update_post_meta( $order_id, 'iwp_marketing_opting', $marketing_opting );
		}
	}

	/**
	 * Output custom fields in order.
	 *
	 * @param object $order Order.
	 */
	public function output_in_order(
		object $order
	): void {
		$terms            = get_post_meta( $order->id, 'iwp_terms', true ) ? __( 'Yes', 'ten' ) : __( 'No', 'ten' );
		$personal         = get_post_meta( $order->id, 'iwp_terms', true ) ? __( 'Yes', 'ten' ) : __( 'No', 'ten' );
		$marketing_opting = get_post_meta( $order->id, 'iwp_marketing_opting', true ) ? __( 'Yes', 'north' ) : __( 'No', 'ten' );

		echo '<p><strong>' . esc_html( __( 'Terms and Conditions', 'ten' ) ) . ':</strong> ' . esc_html( $terms ) . '</p>';
		echo '<p><strong>' . esc_html( __( 'Personal Data', 'ten' ) ) . ':</strong> ' . esc_html( $personal ) . '</p>';
		echo '<p><strong>' . esc_html( __( 'Marketing Opting', 'ten' ) ) . ':</strong> ' . esc_html( $marketing_opting ) . '</p>';
	}

	/**
	 * Show next / prev product.
	 *
	 * @return void
	 */
	public function show_next_prev_product() {
		$previous = get_next_post_link( '%link', '', true, ' ', 'product_cat' );
		$next     = get_previous_post_link( '%link', '', true, ' ', 'product_cat' );
		echo str_replace( '<a', '<a class="icon-right "', $previous );
		echo str_replace( '<a', '<a class="icon-left "', $next );
	}

	/**
	 * Fix error WPML woocommerce_admin_disabled.
	 *
	 * @return void
	 */
	public function fix_wpml_error() {
		remove_all_filters( 'woocommerce_admin_disabled' );
	}

	/**
	 * Set cookie to back url.
	 *
	 * @return void
	 */
	public function set_cookie() {
		$url = '';
		if ( ! empty( $_SERVER['HTTP_REFERER'] ) ) {
			preg_match( '/(?:[^:]+:\/\/)?([^.]+\.)*([^.]+)\.([a-z.]+)/', $_SERVER['HTTP_REFERER'], $matches );
			$url = $matches[0] === get_bloginfo( 'url' ) ? $matches[0] : '';
		}

		if ( empty( $url ) ) {
			return;
		}

		setcookie( "back_to_url", esc_html( $_SERVER['HTTP_REFERER'] ), time() + 3600 * 24, '/' );
	}

	/**
	 * Marge IDs Gallery and variation Image IDs.
	 *
	 * @param array $variation   Woocommerce Variation Products.
	 * @param array $gallery_ids Gallery Image IDs.
	 *
	 * @return array
	 */
	public static function get_image_ids( $variation, $gallery_ids ): array {

		$image_ids = [];

		foreach ( $variation as $image_id ) {
			$image_ids[] = $image_id['image_id'];
		}

		$image_ids = array_merge( $image_ids, $gallery_ids );

		$image_ids = array_unique( $image_ids, SORT_NUMERIC );

		return $image_ids;
	}

	/**
	 * Get variation image ids by color
	 *
	 * @param array $variation Woocommerce Variation Products.
	 *
	 * @return array [color_slug]-> Image id.
	 */
	public static function get_variation_image_ids( $variation ): array {
		$image_ids = [];

		foreach ( $variation as $item ) {
			$image_ids[ $item['attributes']['attribute_pa_color'] ] = $item['image_id'];
		}

		return $image_ids;
	}
}
