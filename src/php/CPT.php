<?php
/**
 * Add Custom Post Type Influences.
 *
 * @package TEN
 */

namespace TEN;

/**
 * CPT class file.
 */
class CPT {
	/**
	 * CPT construct.
	 */
	public function __construct() {
		$this->register_post_types();
	}

	/**
	 * Register CPT.
	 *
	 * @return void
	 */
	private function register_post_types(): void {
		register_post_type(
			'influences',
			[
				'label'         => null,
				'labels'        => [
					'name'               => __( 'Influencerzy', 'ten' ),
					'singular_name'      => __( 'Influencerzy', 'ten' ),
					'add_new'            => __( 'Dodaj Influencerzy', 'ten' ),
					'add_new_item'       => __( 'Dodawanie Influencerzy', 'ten' ),
					'edit_item'          => __( 'Edycja Influencerzy', 'ten' ),
					'new_item'           => __( 'Nowi Influencerzy', 'ten' ),
					'view_item'          => __( 'Oglądaj Influencerzy', 'ten' ),
					'search_items'       => __( 'Szukaj Influencerzy', 'ten' ),
					'not_found'          => __( 'Nie znaleziono', 'ten' ),
					'not_found_in_trash' => __( 'Nie znaleziono w koszyku', 'ten' ),
					'parent_item_colon'  => '',
					'menu_name'          => __( 'Influencerzy', 'ten' ),
				],
				'description'   => '',
				'public'        => true,
				'show_in_menu'  => true,
				'menu_position' => 40,
				'menu_icon'     => 'dashicons-universal-access-alt',
				'hierarchical'  => false,
				'supports'      => [ 'title', 'editor', 'thumbnail', 'excerpt', 'custom-fields', 'revisions' ],
				'taxonomies'    => [],
				'has_archive'   => false,
				'rewrite'       => true,
				'query_var'     => true,
			]
		);
	}
}
