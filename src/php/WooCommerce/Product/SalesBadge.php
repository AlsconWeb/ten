<?php
/**
 * Add Sales badge action.
 *
 * @package TEN\WooCommerce\Product
 */

namespace TEN\WooCommerce\Product;

/**
 * SalesBadge class file
 */
class SalesBadge {

	/**
	 * SalesBadge construct
	 */
	public function __construct() {
		add_action( 'sales_badge', [ $this, 'show_sales_budge' ], 10 );
	}

	/**
	 * Output Badge Html.
	 *
	 * @return void
	 */
	public function show_sales_budge(): void {
		global $product;

		if ( $product->is_on_sale() && empty( $product->get_sale_price() ) ) {

			echo '<p class="sale-budge">' . esc_html( esc_html__( 'Sale!', 'woocommerce' ) ) . '</p>';
		}

		if ( ! empty( $product->get_sale_price() ) ) {
			$text = $this->get_percent_sales();
			echo '<p class="sale-budge">-' . esc_html( $text ) . '%</p>';
		}
	}

	/**
	 * Get Percent sales.
	 *
	 * @return float
	 */
	public function get_percent_sales(): float {
		global $product;

		$price       = $product->get_regular_price();
		$sales_price = $product->get_sale_price();

		return floor( 100 - ( $sales_price * 100 / $price ) );
	}
}
