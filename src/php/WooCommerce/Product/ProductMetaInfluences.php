<?php
/**
 * Product meta influences.
 *
 * @package TEN\WooCommerce\Product
 */

namespace TEN\WooCommerce\Product;

/**
 * ProductMetaInfluences class file.
 */
class ProductMetaInfluences {
	/**
	 * ProductMetaInfluences construct.
	 */
	public function __construct() {
		add_action( 'woocommerce_before_shop_loop_item_title', [ $this, 'show_product_meta' ], 30 );
	}

	/**
	 * Show product meta influences
	 *
	 * @return void
	 */
	public function show_product_meta(): void {

		$mata = $this->get_product_meta();
		if ( isset( $mata['influencer'][0]['id'] ) ) {
			echo sprintf(
				'<p class="meta" style="background-color: %s"><a href="%s">%s</a></p>',
				esc_attr( $mata['color'] ),
				esc_url( get_the_permalink( $mata['influencer'][0]['id'] ) ),
				esc_html( get_the_title( $mata['influencer'][0]['id'] ) )
			);
		}

	}

	/**
	 * Get Product meta influences.
	 *
	 * @return array
	 */
	public function get_product_meta(): array {
		global $product;

		$product_id       = $product->get_id();
		$product_category = wp_get_post_terms( $product_id, 'product_cat' );
		$term_meta        = [];
		foreach ( $product_category as $category ) {
			$influencer = carbon_get_term_meta( $category->term_id, 'ten_association_influences' );
			if ( ! empty( $influencer ) ) {
				$term_meta['color']      = carbon_get_term_meta( $category->term_id, 'influencer_color' );
				$term_meta['influencer'] = $influencer;
				$term_meta['url']        = get_term_link( $category->term_id );
			}
		}

		return $term_meta;
	}
}
