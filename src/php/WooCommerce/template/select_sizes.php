<?php
/**
 * Template select sizes.
 */

global $product;
if ( ! empty( $available_variation['all']['attribute_pa_sizes'] ) ) :
	asort( $available_variation['all']['attribute_pa_sizes'] );
	global $product;
	$default_size = $product->get_default_attributes()['pa_sizes'] ?? '';
	?>
	<div class="select">
		<?php if ( is_singular( 'product' ) ) : ?>
			<label><?php esc_attr_e( 'Rozmiar', 'ten' ); ?></label>
		<?php endif; ?>
		<select
				name="sizes"
				data-product-id="<?php echo esc_attr( $product->get_id() ); ?>">
			<option value="0"><?php echo esc_attr__( 'Rozmiary', 'ten' ); ?></option>
			<?php foreach ( $available_variation['all']['attribute_pa_sizes'] as $key => $value ) : ?>
				<option value="<?php echo esc_html( $value ); ?>" <?php echo $default_size === $value ? 'selected' : ''; ?>>
					<?php echo esc_html( $value ); ?>
				</option>
			<?php endforeach; ?>
		</select>
	</div>
<?php
endif;