<?php
/**
 * Template select color.
 *
 * @package TEN
 */

use TEN\WooCommerceInit;

if ( ! empty( $available_variation['all']['attribute_pa_color'] ) ) :
	asort( $available_variation['all']['attribute_pa_color'] );
	global $product;

	$images_ids = WooCommerceInit::get_variation_image_ids( $product->get_available_variations() );
	?>
	<div
			class="radio-items"
			data-attributes='<?php echo wp_json_encode( $available_variation['all_attributes'] ); ?>'
			data-default-color='<?php echo wp_json_encode( $default_color ); ?>'
			data-prod_id="<?php echo esc_attr( $product->get_id() ); ?>"
			data-default-attributes='<?php echo wp_json_encode( $product->get_default_attributes() ) ?? ''; ?>'
	>
		<?php if ( is_singular( 'product' ) ) : ?>
			<label><?php esc_attr_e( 'Kolor', 'ten' ); ?></label>
		<?php endif; ?>
		<?php foreach ( $available_variation['all']['attribute_pa_color'] as $color ) : ?>
			<?php
			$term_color = get_term_by( 'slug', $color, 'pa_color' );
			$color_code = carbon_get_term_meta( $term_color->term_id, 'product_color' );
			?>
			<div class="radio">
				<input
						id="<?php echo esc_attr( $term_color->slug . '-' . $term_color->term_id . '-' . $product->get_id() ); ?>"
						type="radio"
						value="<?php echo esc_attr( $term_color->slug ); ?>"
						name="color-<?php echo esc_attr( $product->get_id() ); ?>"
						data-image-id="<?php echo esc_attr( $images_ids[ $term_color->slug ] ); ?>"
				>
				<label
						for="<?php echo esc_attr( $term_color->slug . '-' . $term_color->term_id . '-' . $product->get_id() ); ?>"
						style="color:<?php echo esc_attr( $color_code ?? '' ); ?>;">
				</label>
			</div>
		<?php endforeach; ?>
	</div>
<?php
endif;
