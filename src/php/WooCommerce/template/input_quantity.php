<?php
/**
 * Template input quantity.
 *
 * @package TEN
 */

?>
<div class="input">
	<label><?php esc_attr_e( 'Ilość', 'ten' ); ?></label>
	<input type="number" class="quantity" name="quantity" value="1">
</div>
