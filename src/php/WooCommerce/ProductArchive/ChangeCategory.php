<?php
/**
 * Add Action Category Change and Load more product.
 *
 * @package TEN\WooCommerce\ProductArchive
 */

namespace TEN\WooCommerce\ProductArchive;

/**
 * ChangeCategory class file.
 */
class ChangeCategory {

	/**
	 * ChangeCategory construct.
	 */
	public function __construct() {
		add_action( 'show_category', [ $this, 'show_category_html' ], 10 );
		add_action( 'show_category', [ $this, 'show_button_load_more' ], 20 );
	}

	/**
	 * Show category name and change category
	 *
	 * @return void
	 */
	public function show_category_html(): void {
		$term_name = '';
		$influence = '';
		$query_obj = '';

		if ( ! is_search() ) {
			$query_obj = get_queried_object();
			$term_name = $query_obj->name;
			$influence = carbon_get_term_meta( $query_obj->term_id, 'ten_association_influences' );
		}

		if ( is_search() ) {
			$term_name = get_search_query();
		}

		if ( 'product' === $term_name ) {
			$term_name = __( 'Sklep', 'ten' );
		}

		if ( ! empty( $influence ) ) {
			$categories = $this->get_influence_category( $query_obj->slug );
			?>
			<h1 class="title">
				<?php esc_html_e( 'Kolekcja', 'ten' ); ?>
				<span><?php echo esc_html( $query_obj->name ); ?></span>
			</h1>
			<ul class="category-link">
				<?php foreach ( $categories as $category ) : ?>
					<li class="current-menu-item">
						<a href="<?php echo esc_url( get_term_link( $category->term_id ) ); ?>">
							<?php echo esc_html( $category->name ); ?>
						</a>
					</li>
				<?php endforeach; ?>
			</ul>
			<?php
		} else {
			echo sprintf(
				'<h1 class="title">%s<span class="icon-edited">%s</span></h1>',
				esc_html( __( 'Wyniki dla:', 'ten' ) ),
				esc_html( $term_name )
			);
		}
	}

	/**
	 * Output button load more.
	 *
	 * @return void
	 */
	public function show_button_load_more(): void {

		$count_in_category = get_queried_object();

		if ( 4 < $count_in_category->count ) {
			echo sprintf(
				"<a class='%s' id='%s' href='#'>%s</a>",
				'icon-right',
				'next-slide',
				/* translators: %s: text button*/
				esc_html( __( 'Przewiń po więcej', 'ten' ) )
			);
		}
	}

	/**
	 * Gets all categories influencer.
	 *
	 * @param string $influence_name Term Slug.
	 *
	 * @return array
	 */
	private function get_influence_category( string $influence_name ): array {
		$all_posts = new \WP_Query(
			[
				'post_type'      => 'product',
				'posts_per_page' => - 1,
				'tax_query'      => [
					[
						'taxonomy' => 'product_cat',
						'field'    => 'slug',
						'terms'    => $influence_name,
					],
				],
			]
		);

		$all_category = [];
		foreach ( $all_posts->posts as $key => $post ) {
			$post_category = get_the_terms( $post->ID, 'product_cat' );
			foreach ( $post_category as $category ) {
				$all_category[] = $category;
			}
		}

		if ( empty( $all_category ) ) {
			return [];
		}


		if ( function_exists( 'array_unique_recursive' ) ) {
			$all_category = array_unique_recursive( $all_category );
		} else {
			$all_category = $all_category[0];
		}

		$all_category = array_values( array_column( $all_category, null, 'slug' ) );

		foreach ( $all_category as $key => $category ) {
			if ( $influence_name === $category->slug ) {
				unset( $all_category[ $key ] );
			}
		}

		usort( $all_category, function ( $a, $b ) {
			return strcmp( $a->name, $b->name );
		} );

		return $all_category;
	}
}
