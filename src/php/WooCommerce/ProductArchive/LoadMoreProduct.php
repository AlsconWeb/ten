<?php
/**
 * Ajax Handler Load More product.
 *
 * @package TEN\WooCommerce\ProductArchive
 */

namespace TEN\WooCommerce\ProductArchive;

/**
 * LoadMoreProduct class file.
 */
class LoadMoreProduct {

	/**
	 * LoadMoreProduct construct
	 */
	public function __construct() {
		add_action( 'wp_ajax_lode_more', [ $this, 'ajax_handler_load_more' ] );
		add_action( 'wp_ajax_nopriv_lode_more', [ $this, 'ajax_handler_load_more' ] );
	}

	/**
	 * Ajax Handler Load More.
	 *
	 * @return void
	 */
	public function ajax_handler_load_more(): void {

		$nonce = isset( $_POST['nonce'] ) ? filter_var( wp_unslash( $_POST['nonce'] ), FILTER_SANITIZE_STRING ) : null;
		$page  = isset( $_POST['page'] ) ? filter_var( wp_unslash( $_POST['page'] ), FILTER_SANITIZE_NUMBER_INT ) : null;

		if ( ! isset( $_POST['query'] ) ) {
			wp_send_json_error( [ 'message' => __( 'Nie jest to dobrze znany błąd żądania', 'ten' ) ] );
		}

		if ( ! wp_verify_nonce( $nonce, 'load-more-nonce' ) ) {
			wp_send_json_error( [ 'message' => __( 'Nieprawidłowy kod jednorazowy', 'ten' ) ] );
		}

		//phpcs:disable
		$arg = $this->parse_query_args( wp_unslash( $_POST['query'] ) );
		//phpcs:enable
		ob_start();
		if ( ! empty( $arg ) ) {
			$arg['paged'] = (int) $page + 1;
			$query        = new \WP_Query( $arg );

			if ( $query->have_posts() ) {
				while ( $query->have_posts() ) :
					$query->the_post();
					wc_get_template_part( 'content', 'product' );
				endwhile;
				wp_reset_postdata();
			}
		}

		wp_send_json_success(
			[
				'html'  => ob_get_clean(),
				'paged' => $arg['paged'],
			]
		);
	}

	/**
	 * Parse Query.
	 *
	 * @param array $query $wp_query->query.
	 *
	 * @return array
	 */
	private function parse_query_args( array $query ): array {
		$arg = [];
		foreach ( $query as $key => $value ) {
			switch ( $query ) {
				case 'product_cat' === $key:
					$arg['post_type'] = 'product';
					$arg['order']     = 'ASC';
					$arg['orderby']   = 'title';
					$arg[ $key ]      = $value;
					break;
				case 'post_type' === $key:
					$arg['post_type'] = 'product';
					$arg['order']     = 'ASC';
					break;
			}
		}

		if ( ! empty( $arg ) ) {
			return $arg;
		}

		return [];
	}

}
