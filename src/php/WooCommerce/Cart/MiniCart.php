<?php
/**
 * Output products in mini cart popover.
 *
 * @package TEN\WooCommerce\Cart
 */

namespace TEN\WooCommerce\Cart;

/**
 * MiniCart class file.
 */
class MiniCart {
	/**
	 * Total Price.
	 *
	 * @var float
	 */
	protected float $total_price;

	/**
	 * Price for free shipping.
	 *
	 * @var float
	 */
	protected float $free_shipping_price;

	/**
	 * Cart Helpers Class.
	 *
	 * @var ChangeCart Class Cart.
	 */
	protected ChangeCart $cart;

	/**
	 * MiniCart construct.
	 */
	public function __construct() {
		global $woocommerce;

		$this->total_price         = $woocommerce->cart->total;
		$this->cart                = new ChangeCart();
		$this->free_shipping_price = carbon_get_theme_option( 'ten_free_shipping_price' );
	}

	/**
	 * Total price.
	 *
	 * @return float
	 */
	private function get_total(): float {
		return $this->total_price;
	}


	/**
	 * Show Mini Cart.
	 *
	 * @return void
	 */
	public function show_mini_cart(): void {
		global $woocommerce;

		$cart_items = $woocommerce->cart->get_cart();
		if ( ! empty( $cart_items ) ) :
			?>
			<div class="mini-cart-wrapper">
				<div class="cart-items">
					<?php foreach ( $cart_items as $cart_item_key => $cart_item ) : ?>
						<?php
						$product     = $cart_item['data'];
						$product_id  = $cart_item['product_id'];
						$color_name  = $product->get_attributes()['pa_color'] ?? null;
						$item_delete = apply_filters(
							'woocommerce_cart_item_remove_link',
							sprintf(
								'<a href="%s" class="icon-close remove" aria-label="%s" data-product_id="%s" data-product_sku="%s" data-cart_item_key="%s"></a>',
								esc_url( wc_get_cart_remove_url( $cart_item_key ) ),
								esc_html__( 'Remove this item', 'deepsoul' ),
								esc_attr( $product_id ),
								esc_attr( $product->get_sku() ),
								esc_attr( $cart_item_key )
							),
							$cart_item_key
						);
						?>
						<div class="cart-item">
							<?php
							if ( has_post_thumbnail( $product_id ) ) :
								echo get_the_post_thumbnail( $product_id, 'full' );
								?>
							<?php else : ?>
								<img src="https://via.placeholder.com/133x133?text=No+image" alt="No IMage">
							<?php endif ?>
							<div class="description">
								<h5 class="title"><?php echo esc_html( get_the_title( $product_id ) ); ?></h5>
								<ul class="product-meta">
									<li class="price">
										<?php echo wp_kses_post( WC()->cart->get_product_price( $product ) ); ?>
									</li>
									<?php if ( $color_name ) : ?>
										<li class="color">
											<?php esc_html_e( 'Kolor', 'ten' ); ?>
											<span style="color:<?php echo esc_attr( $this->cart->get_color_product( $color_name ) ); ?>;"></span>
										</li>
									<?php endif ?>
									<li class="quantity">
										<?php esc_html_e( 'Ilość', 'ten' ); ?>
										<span><?php echo esc_attr( $cart_item['quantity'] ); ?></span>
									</li>
								</ul>
								<?php echo wp_kses_post( $item_delete ); ?>
							</div>
						</div>
					<?php endforeach; ?>
				</div>
				<?php
				if ( $this->free_shipping_price > 0 ) :
					if ( $this->free_shipping_price <= $this->get_total() ) :
						?>
						<h4 class="title"><?php esc_html_e( 'Darmowa dostawa', 'ten' ); ?></h4>
					<?php else : ?>
						<h5 class="title">
							<?php
							echo wp_kses_post(
								sprintf(
								/* translators: %d: left until free shipping %s: currency symbol*/
									__( 'Do darmowej dostawy brakuje Ci jeszcze %1$d %2$s', 'ten' ),
									$this->cart->get_left_to_free_shipping( $this->get_total() ),
									get_woocommerce_currency_symbol()
								)
							);
							?>
						</h5>
					<?php endif; ?>
				<?php endif; ?>
				<h4 class="price">
					<?php esc_html_e( 'Razem do zapłaty:', 'ten' ); ?>
					<span><?php echo wp_kses_post( WC()->cart->get_cart_total() ); ?></span>
				</h4>
				<ul class="buttons">
					<li>
						<a class="button" href="<?php echo esc_url( wc_get_cart_url() ); ?>">
							<?php esc_html_e( 'Idź do koszyka', 'ten' ); ?>
						</a>
					</li>
					<li>
						<a class="button" href="<?php echo esc_url( wc_get_checkout_url() ); ?>">
							<?php esc_html_e( 'Idź do kasy', 'ten' ); ?>
						</a>
					</li>
				</ul>
			</div>
		<?php
		else :
			?>
			<div class="mini-cart-wrapper">
				<h4 class="title"><?php esc_html_e( 'Koszyk jest pusty', 'ten' ); ?></h4>
			</div>
		<?php
		endif;
	}
}
