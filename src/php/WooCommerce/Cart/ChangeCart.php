<?php
/**
 * Adds or changes cart.
 *
 * @package TEN\WooCommerce\Cart
 */

namespace TEN\WooCommerce\Cart;

/**
 * ChangeCart class file.
 */
class ChangeCart {
	/**
	 * Price for free shipping.
	 *
	 * @var float
	 */
	protected float $free_shipping_price = 0;

	/**
	 * Get color code.
	 *
	 * @param string $term_slug Product attribute slug.
	 *
	 * @return string color
	 */
	public function get_color_product( string $term_slug ): string {
		$term_id = get_term_by( 'slug', $term_slug, 'pa_color' )->term_id;

		return carbon_get_term_meta( $term_id, 'product_color' );
	}

	/**
	 * Get left to free shipping.
	 *
	 * @param float $total Total price.
	 *
	 * @return float left count.
	 */
	public function get_left_to_free_shipping( float $total ): float {
		$this->free_shipping_price = carbon_get_theme_option( 'ten_free_shipping_price' );
		$total                     = $this->free_shipping_price - $total;

		return max( (float) $total, 0 );
	}

	/**
	 * Output cart header.
	 */
	public function show_cart_header(): void {
		$items_count = WC()->cart->get_cart_contents_count();
		?>
		<div class="top-head">
			<?php
			echo sprintf(
				'<h1 class="title">%s <span>(%d)</span></h1>',
				/* translators: %s: Text  %d: count product in cart */
				esc_html( __( 'Twój koszyk', 'ten' ) ),
				esc_attr( $items_count )
			);
			?>
			<a class="icon-left" href="<?php echo esc_url( get_the_permalink( wc_get_page_id( 'shop' ) ) ); ?>">
				<?php esc_html_e( 'Powrót do sklepu', 'ten' ); ?>
			</a>
		</div>
		<?php
	}

	/**
	 * Get
	 *
	 * @return float
	 */
	public function get_free_shipping_price(): float {
		return $this->free_shipping_price;
	}
}
