<?php
/**
 * Theme Init.
 * Theme initialization and customization.
 *
 * @package TEN
 */

namespace TEN;

use Carbon_Fields\Carbon_Fields;
use Carbon_Fields\Container;
use Carbon_Fields\Field;
use TEN\WPBakeryWidgets\Widgets\EventsBlock\TenEventsBlock;
use TEN\WPBakeryWidgets\Widgets\InfluencesSlider\TenInfluencesSlider;
use TEN\WPBakeryWidgets\Widgets\Social\TenSocialNetworks;
use WP_Query;

/**
 * ThemeInit class file.
 */
class ThemeInit {

	/**
	 * ThemeInit construct.
	 */
	public function __construct() {
		$this->theme_hook_init();
	}

	/**
	 * Initializing hocks and actions themes.
	 *
	 * @return void
	 */
	private function theme_hook_init(): void {
		add_action( 'wp_enqueue_scripts', [ $this, 'add_script' ] );
		add_action( 'after_setup_theme', [ $this, 'add_carbon_fields' ] );
		add_action( 'after_setup_theme', [ $this, 'add_theme_supports' ], 15 );
		add_action( 'carbon_fields_register_fields', [ $this, 'add_theme_settings' ] );
		add_filter( 'upload_mimes', [ $this, 'add_mime_types' ] );
		add_action( 'custom_switcher', [ $this, 'get_languages_switcher' ] );
		add_action( 'vc_before_init', [ $this, 'add_bakery_components' ] );
		add_action( 'init', [ $this, 'add_cpt' ] );
		add_action( 'get_pagination_post', [ $this, 'get_pagination_post' ] );
		add_filter( 'pre_get_posts', [ $this, 'ten_search_filter_pages' ] );

		new WooCommerceInit();
	}

	/**
	 * Add Script and Style.
	 *
	 * @return void
	 */
	public function add_script(): void {
		wp_enqueue_script(
			'build',
			TEN_THEME_URL . '/assets/js/build.js',
			[
				'jquery',
			],
			TEN_THEME_VERSTION,
			true
		);
		wp_enqueue_script(
			'main',
			TEN_THEME_URL . '/assets/js/main.min.js',
			[
				'jquery',
			],
			TEN_THEME_VERSTION,
			true
		);

		wp_enqueue_script( 'fancyapps', '//cdn.jsdelivr.net/npm/@fancyapps/ui@4.0/dist/fancybox.umd.js', [ 'jquery' ], '4.0' );

		wp_enqueue_style( 'main', TEN_THEME_URL . '/assets/css/main.css', [], TEN_THEME_VERSTION );
		wp_enqueue_style( 'style', TEN_THEME_URL . '/style.css', [], TEN_THEME_VERSTION );
		wp_enqueue_style( 'fancyapps', '//cdn.jsdelivr.net/npm/@fancyapps/ui@4.0/dist/fancybox.css', [], '4.0' );
	}

	/**
	 * Init Carbon Fields.
	 *
	 * @return void
	 */
	public function add_carbon_fields(): void {
		Carbon_Fields::boot();
	}

	/**
	 * Add Theme Settings.
	 *
	 * @return void
	 */
	public function add_theme_settings(): void {

		/**
		 * Theme Option.
		 */
		$options_container = Container::make(
			'theme_options',
			__( 'Opcje motywu', 'ten' )
		)->set_icon( 'dashicons-admin-settings' )
			->add_fields(
				[
					Field::make( 'image', 'ten_logo', __( 'Logo', 'ten' ) )
						->set_help_text( __( 'Ustaw obraz logo', 'ten' ) )
						->set_width( 50 )
						->set_value_type( 'url' ),

					Field::make( 'image', 'ten_logo_white', __( 'Logo Białe', 'ten' ) )
						->set_help_text( __( 'Ustaw biały obraz logo', 'ten' ) )
						->set_width( 50 )
						->set_value_type( 'url' ),

					Field::make( 'text', 'ten_google_front', __( 'Czcionka Google', 'ten' ) )
						->set_attribute( 'placeholder', __( 'adres URL google front', 'ten' ) )
						->set_default_value( 'https://fonts.googleapis.com/css2?family=Chivo:wght@400;700&amp;family=Poppins:wght@400;700&amp;family=Roboto:wght@400;700&amp;display=swap' ),

					Field::make( 'footer_scripts', 'ten_footer_scripts', __( 'Skrypty analizy stopki', 'ten' ) ),

				]
			);

		/**
		 * Social option.
		 */
		Container::make( 'theme_options', __( 'Social Links', 'ten' ) )
			->set_page_parent( $options_container )
			->add_fields(
				[
					Field::make( 'text', 'ten_facebook_link', __( 'Facebook Link', 'ten' ) ),
					Field::make( 'text', 'ten_twitter_link', __( 'Twitter Link', 'ten' ) ),
					Field::make( 'text', 'ten_instagram_link', __( 'Instagram Link', 'ten' ) ),
				]
			);

		/**
		 * Woocommerce option.
		 */
		Container::make( 'theme_options', __( 'WooCommerce', 'ten' ) )
			->set_page_parent( $options_container )
			->add_fields(
				[
					Field::make( 'text', 'ten_free_shipping_price', __( 'Cena za darmową wysyłkę.', 'ten' ) )
						->set_help_text( __( 'Jeżeli wartość wynosi 0, powiadomienie o dostarczeniu opłaty bazowej zależnej od kwoty jest wyłączone', 'ten' ) )
						->set_attribute( 'type', 'number' )
						->set_attribute( 'min', 0 )
						->set_default_value( 0 ),
				]
			);

		/**
		 * 404 page option.
		 */
		Container::make( 'theme_options', __( 'Strona 404', 'ten' ) )
			->set_page_parent( $options_container )
			->add_fields(
				[
					Field::make( 'rich_text', 'ten_404_text', __( 'Tekst strony 404', 'ten' ) ),
				]
			);

		/**
		 * Term product attributes
		 */
		Container::make( 'term_meta', __( 'Właściwości kategorii', 'ten' ) )
			->where( 'term_taxonomy', '=', 'pa_color' )
			->add_fields(
				[
					Field::make( 'color', 'product_color', __( 'Kolor produktu', 'ten' ) )
						->set_palette(
							[
								'#FF0000',
								'#000000',
								'#0000FF',
								'#FFFFFF',
								'#F16C4F',
								'#B2B2B3',
							]
						),
				]
			);

		Container::make( 'term_meta', __( 'Właściwości kategorii', 'ten' ) )
			->where( 'term_taxonomy', '=', 'product_cat' )
			->add_fields(
				[
					Field::make( 'color', 'influencer_color', __( 'Kolor Influencerzy', 'ten' ) )
						->set_palette(
							[
								'#FF0000',
								'#000000',
								'#0000FF',
								'#FFFFFF',
								'#F16C4F',
								'#B2B2B3',
							]
						),
					Field::make( 'association', 'ten_association_influences', __( 'Kategoria produktu stowarzyszenia', 'ten' ) )
						->set_max( 1 )
						->set_types(
							[
								[
									'type'      => 'post',
									'post_type' => 'influences',
								],
							]
						),
				]
			);

		/**
		 * Meta box in CPT influences.
		 */
		Container::make( 'post_meta', __( 'Kategoria produktów Woocommerce', 'ten' ) )
			->where( 'post_type', '=', 'influences' )
			->set_priority( 'high' )
			->add_fields(
				[
					Field::make( 'association', 'ten_association_product', __( 'Wybierz kategorię produkt', 'ten' ) )
						->set_min( 1 )
						->set_max( 1 )
						->set_types(
							[
								[
									'type'     => 'term',
									'taxonomy' => 'product_cat',
								],
							]
						),
					Field::make( 'image', 'influences_photo', __( 'Duży obraz', 'ten' ) )
						->set_help_text( __( 'Zdjęcie jest wyświetlane w biografii jako tytuł', 'ten' ) )
						->set_value_type( 'url' ),
				]
			);
	}

	/**
	 * Theme Support.
	 *
	 * @return void
	 */
	public function add_theme_supports(): void {
		add_theme_support( 'woocommerce' );
		add_theme_support( 'wc-product-gallery-lightbox' );
		add_theme_support( 'wc-product-gallery-slider' );
		add_theme_support( 'wc-product-gallery-zoom' );

		$this->add_text_domain();

		register_nav_menus(
			[
				'header_menu'        => __( 'Menu nagłówka', 'ten' ),
				'header_menu_button' => __( 'Menu przycisku nagłówka', 'ten' ),
				'in_search_pop_up'   => __( 'W menu wyszukiwania', 'ten' ),
			]
		);

	}

	/**
	 * Add Support SVG format.
	 *
	 * @param array $mimes Mimes Type.
	 *
	 * @return array
	 */
	public function add_mime_types( array $mimes ): array {
		$mimes['svg'] = 'image/svg+xml';

		return $mimes;
	}

	/**
	 * Add Russian Localization
	 */
	private function add_text_domain(): void {
		load_theme_textdomain( 'ten', get_template_directory() . '/languages' );
	}

	/**
	 * Output custom swicher.
	 */
	public function get_languages_switcher(): void {
		if ( function_exists( 'icl_get_languages' ) ) {
			$languages = icl_get_languages( 'skip_missing=0&orderby=code' );
			if ( ! empty( $languages ) ) {
				ob_start();
				$active_lang = '';
				foreach ( $languages as $language ) {
					if ( $language['active'] ) {
						$active_lang = $language;
					}
				}

				?>
				<div
						class="language <?php echo 'en' !== ICL_LANGUAGE_CODE ? '' : 'checked'; ?>"
						data-active_lang="<?php echo esc_attr( $active_lang['code'] ); ?>"
						data-url_pl="<?php echo esc_url( $languages['pl']['url'] ); ?>"
						data-url_en="<?php echo esc_url( $languages['en']['url'] ); ?>"
				>
					<label class="en" for="language"
						   id="lang-text"><?php ICL_LANGUAGE_CODE !== 'en' ? esc_html_e( 'En', 'ten' ) : esc_html_e( 'Pl', 'ten' ); ?></label>
					<input id="language" type="checkbox" <?php echo 'en' !== ICL_LANGUAGE_CODE ? '' : 'checked'; ?>>
					<label for="language"></label>
				</div>
				<?php
				// phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
				echo ob_get_clean();
				// phpcs:enable WordPress.Security.EscapeOutput.OutputNotEscaped
			}
		}
	}

	/**
	 * Add WPBakery components.
	 *
	 * @return void
	 */
	public function add_bakery_components(): void {
		new TenEventsBlock();
		new TenInfluencesSlider();
		new TenSocialNetworks();
	}

	/**
	 * Add Custom post type Influences
	 *
	 * @return void
	 */
	public function add_cpt(): void {
		new CPT();
	}


	/**
	 * Loop pagination arrow in post type influences
	 *
	 * @return void
	 */
	public function get_pagination_post(): void {

		if ( get_adjacent_post( false, '', true ) ) {
			previous_post_link( '%link', '' );
		} else {
			$first = new WP_Query(
				[
					'posts_per_page' => 1,
					'order'          => 'DESC',
					'post_type'      => 'influences',
				]
			);
			$first->the_post();

			echo wp_kses_post( '<a href="' . get_permalink() . '" class="icon-left"></a>' );

			wp_reset_postdata();
		}

		if ( get_adjacent_post( false, '', false ) ) {
			next_post_link( '%link', '' );
		} else {
			$last = new WP_Query(
				[
					'posts_per_page' => 1,
					'order'          => 'ASC',
					'post_type'      => 'influences',
				]
			);
			$last->the_post();

			echo wp_kses_post( '<a href="' . get_permalink() . '" class="icon-right"></a>' );

			wp_reset_postdata();
		}
	}

	/**
	 * Add filter search.
	 *
	 * @param WP_Query $query Query.
	 *
	 * @return mixed
	 */
	public function ten_search_filter_pages( $query ) {

		if ( ! is_admin() && $query->is_search() ) {
			$query->set( 'post_type', 'product' );
			$query->set( 'wc_query', 'product_query' );
		}

		return $query;
	}
}
