<?php
/**
 * Template EventsBlock Widget.
 *
 * @package TEN\WPBakeryWidgets\Widgets\EventsBlock
 */

$ten_title = $atts['title'];
$image     = wp_get_attachment_image_url( $atts['image'], 'medium' );
$ten_link  = vc_build_link( $atts['url'] );
$css_class = vc_shortcode_custom_css_class( $atts['css'] ?? '', ' ' );
?>

<div class="event <?php echo esc_attr( $css_class ?? null ); ?>">
	<div class="img icon-x">
		<img
				src="<?php echo esc_url( $image ); ?>"
				alt="<?php echo esc_attr( get_the_title( $atts['image'] ) ); ?>"
				class="attachment-medium">
	</div>
	<div class="description">
		<h1 class="title">
			<?php echo esc_html( $ten_title ); ?>
		</h1>
		<a
				class="icon-chevron link"
				href="<?php echo esc_url( $ten_link['url'] ?? '#' ); ?>"
				rel="<?php echo esc_attr( $ten_link['rel'] ?? null ); ?>"
				target="<?php echo esc_attr( $ten_link['target'] ?? null ); ?>"
		>
			<?php echo esc_html( $ten_link['title'] ); ?>
		</a>
	</div>
</div>
