<?php
/**
 * Template Influences Slider Widget.
 *
 * @package TEN\WPBakeryWidgets\Widgets\InfluencesSlider
 */

$css_class  = vc_shortcode_custom_css_class( $atts['css'] ?? '', ' ' );
$influences = explode( ',', $atts['influences'] );
?>
<div class="home-slider <?php echo esc_attr( $css_class ?? null ); ?>">
	<?php if ( ! empty( $influences ) ) : ?>
		<?php foreach ( $influences as $influence ) : ?>
			<div class="item">
				<a href="<?php echo esc_url( get_the_permalink( $influence ) ); ?>">
					<?php
					if ( has_post_thumbnail( $influence ) ) {
						echo get_the_post_thumbnail( $influence, 'full' );
					}
					?>
				</a>
				<div class="description">
					<h2 class="title">
						<a href="<?php echo esc_url( get_the_permalink( $influence ) ); ?>">
							<?php echo esc_html( get_the_title( $influence ) ); ?>
						</a>
					</h2>
					<a
							class="button"
							href="<?php echo esc_url( get_the_permalink( $influence ) ); ?>">
						<?php esc_html_e( 'Zobacz kolekcję', 'ten' ); ?>
					</a>
				</div>
			</div>
		<?php endforeach; ?>
	<?php else : ?>
		<?php esc_html_e( 'Nie zaznaczaj wpływów do wyjścia', 'ten' ); ?>
	<?php endif; ?>
</div>
