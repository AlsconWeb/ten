<?php
/**
 * Template Ten Social Networks Widget.
 *
 * @package TEN\WPBakeryWidgets\Widgets\Social
 */

$css_class = vc_shortcode_custom_css_class( $atts['css'] ?? '', ' ' );
$socials   = vc_param_group_parse_atts( $atts['socials_network'] );

if ( ! empty( $socials ) ) :
	?>
	<ul class="soc <?php echo esc_attr( $css_class ?? null ); ?>">
		<?php foreach ( $socials as $social ) : ?>
			<li class="<?php echo esc_html( $social['icon_name'] ); ?>">
				<a href="<?php echo esc_url( $social['url'] ); ?>"></a>
			</li>
		<?php endforeach; ?>
	</ul>
<?php
endif;
