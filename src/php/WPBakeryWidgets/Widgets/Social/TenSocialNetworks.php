<?php
/**
 * Social Networks Widget.
 *
 * @package TEN\WPBakeryWidgets\Widgets\Social
 */

namespace TEN\WPBakeryWidgets\Widgets\Social;

/**
 * TenSocialNetworks class file.
 */
class TenSocialNetworks {

	/**
	 * TenSocialNetworks construct.
	 */
	public function __construct() {
		add_shortcode( 'ten_social_networks', [ $this, 'output' ] );

		// Map shortcode to Visual Composer.
		if ( function_exists( 'vc_lean_map' ) ) {
			vc_lean_map( 'ten_social_networks', [ $this, 'map' ] );
		}
	}

	/**
	 * Map field.
	 *
	 * @return array
	 */
	public function map(): array {
		return [
			'name'                    => esc_html__( 'Portale społecznościowe', 'ten' ),
			'description'             => esc_html__( 'Portale społecznościowe', 'ten' ),
			'base'                    => 'ten_social_networks',
			'category'                => __( 'Ten', 'ten' ),
			'show_settings_on_create' => false,
			'icon'                    => '',
			'params'                  => [
				[
					'type'        => 'param_group',
					'heading'     => __( 'Sieć społeczna', 'ten' ),
					'param_name'  => 'socials_network',
					'value'       => '',
					'params'      => [
						[
							'type'       => 'textfield',
							'value'      => '',
							'heading'    => __( 'Nazwa sieci społecznościowej', 'ten' ),
							'param_name' => 'name',
						],
						[
							'type'       => 'textfield',
							'value'      => '',
							'heading'    => __( 'Ikona sieci społecznościowej', 'ten' ),
							'param_name' => 'icon_name',
						],
						[
							'type'       => 'textfield',
							'value'      => '',
							'heading'    => __( 'Link do sieci społecznościowej', 'ten' ),
							'param_name' => 'url',
						],
					],
					'admin_label' => false,
					'save_always' => true,
					'group'       => __( 'Ogólny', 'ten' ),
				],
				[
					'type'       => 'css_editor',
					'heading'    => esc_html__( 'Pole CSS', 'ten' ),
					'param_name' => 'css',
					'group'      => esc_html__( 'Opcje projektowe', 'ten' ),
				],
			],
		];
	}

	/**
	 * Output Short Code template
	 *
	 * @param mixed       $atts    Attributes.
	 * @param string|null $content Content.
	 *
	 * @return string
	 */
	public function output( $atts, string $content = null ): string {
		ob_start();

		include __DIR__ . './../../Template/SocialNetworks/template.php';

		return ob_get_clean();
	}
}
