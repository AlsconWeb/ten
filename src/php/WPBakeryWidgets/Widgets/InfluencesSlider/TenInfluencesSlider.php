<?php
/**
 * Add Ten Influences Slider in WPBakery.
 *
 * @package TEN\WPBakeryWidgets\Widgets\InfluencesSlider
 */

namespace TEN\WPBakeryWidgets\Widgets\InfluencesSlider;

/**
 * TenInfluencesSlider class file.
 */
class TenInfluencesSlider {
	/**
	 * TenInfluencesSlider construct.
	 */
	public function __construct() {
		add_shortcode( 'ten_influences_slider', [ $this, 'output' ] );

		// Map shortcode to Visual Composer.
		if ( function_exists( 'vc_lean_map' ) ) {
			vc_lean_map( 'ten_influences_slider', [ $this, 'map' ] );
		}
	}

	/**
	 * Map field.
	 *
	 * @return array
	 */
	public function map(): array {
		return [
			'name'                    => esc_html__( 'Suwak wpływów', 'ten' ),
			'description'             => esc_html__( 'Suwak wpływów', 'ten' ),
			'base'                    => 'ten_influences_slider',
			'category'                => __( 'Ten', 'ten' ),
			'show_settings_on_create' => false,
			'icon'                    => '',
			'params'                  => [
				[
					'type'       => 'autocomplete',
					'class'      => '',
					'heading'    => esc_html__( 'Influencerzy Nazwa', 'alispx' ),
					'param_name' => 'influences',
					'settings'   => [
						'values'   => $this->ten_get_posts_data(),
						'multiple' => true,
						'sortable' => true,
						'groups'   => true,
					],
				],
				[
					'type'       => 'css_editor',
					'heading'    => esc_html__( 'Pole CSS', 'ten' ),
					'param_name' => 'css',
					'group'      => esc_html__( 'Opcje projektowe', 'ten' ),
				],
			],
		];
	}

	/**
	 * Output Short Code template
	 *
	 * @param mixed       $atts    Attributes.
	 * @param string|null $content Content.
	 *
	 * @return string
	 */
	public function output( $atts, string $content = null ): string {
		ob_start();

		include __DIR__ . './../../Template/InfluencesSlider/template.php';

		return ob_get_clean();
	}

	private function ten_get_posts_data( $post_type = 'influences' ) {

		$posts = get_posts( [
			'posts_per_page' => - 1,
			'post_type'      => $post_type,
		] );

		$result = [];
		foreach ( $posts as $post ) {
			$result[] = [
				'value' => $post->ID,
				'label' => $post->post_title,
			];
		}

		return $result;
	}
}
