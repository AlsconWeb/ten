<?php
/**
 * Add Ten Event Widget in WPBakery.
 *
 * @package TEN\WPBakeryWidgets\Widgets\EventsBlock
 */

namespace TEN\WPBakeryWidgets\Widgets\EventsBlock;

/**
 * TenEventsBlock class file.
 */
class TenEventsBlock {
	/**
	 * TenEventsBlock construct.
	 */
	public function __construct() {
		add_shortcode( 'ten_event_block', [ $this, 'output' ] );

		// Map shortcode to Visual Composer.
		if ( function_exists( 'vc_lean_map' ) ) {
			vc_lean_map( 'ten_event_block', [ $this, 'map' ] );
		}
	}

	/**
	 * Map field.
	 *
	 * @return array
	 */
	public function map(): array {
		return [
			'name'                    => esc_html__( 'Blok zdarzeń', 'ten' ),
			'description'             => esc_html__( 'Blok zdarzeń', 'ten' ),
			'base'                    => 'ten_event_block',
			'category'                => __( 'Ten', 'ten' ),
			'show_settings_on_create' => false,
			'icon'                    => '',
			'params'                  => [
				[
					'type'        => 'attach_image',
					'heading'     => __( 'Obraz', 'tem' ),
					'param_name'  => 'image',
					'value'       => '',
					'admin_label' => false,
					'save_always' => true,
					'group'       => __( 'Ogólny', 'ten' ),
				],
				[
					'type'        => 'textfield',
					'heading'     => __( 'Tytuł', 'tem' ),
					'param_name'  => 'title',
					'value'       => '',
					'admin_label' => false,
					'save_always' => true,
					'group'       => __( 'Ogólny', 'ten' ),
				],
				[
					'type'        => 'vc_link',
					'heading'     => __( 'URL Zobacz więcej', 'tem' ),
					'param_name'  => 'url',
					'value'       => '',
					'admin_label' => false,
					'save_always' => true,
					'group'       => __( 'Ogólny', 'ten' ),
				],
				[
					'type'       => 'css_editor',
					'heading'    => esc_html__( 'Pole CSS', 'ten' ),
					'param_name' => 'css',
					'group'      => esc_html__( 'Opcje projektowe', 'ten' ),
				],
			],
		];
	}

	/**
	 * Output Short Code template
	 *
	 * @param mixed       $atts    Attributes.
	 * @param string|null $content Content.
	 *
	 * @return string
	 */
	public function output( $atts, string $content = null ): string {
		ob_start();
		include __DIR__ . './../../Template/EventsBlock/template.php';

		return ob_get_clean();
	}
}
