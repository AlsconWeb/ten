jQuery(document).ready(function($){
    var heightWindow = $(window).height();
    if($(window).width() > 767){
        if($('.left-block, .single-product').length){
            $('header .logo').hide();
        }
    }
    $(window).on('resize', function () {
        if($('.left-block, .single-product').length){
            if($(window).width() > 767){
                $('header .logo').hide();
            }else{
                $('header .logo').show();
            }
        }
    });
    /*Language checkbox add class start*/
    $('#language:checked').parents('.language').addClass('checked');
    $('#language').change(function(){
        if($(this).prop('checked') == true){
            $(this).parents('.language').addClass('checked');
        }else{
            $(this).parents('.language').removeClass('checked');
        }
    });
    /*Language checkbox add class end*/
    $('.icon-edited').click(function(){
        $('.search').addClass('open');
    });
    /*Open menu start*/
    $('.burger-menu, i.icon-search, .icon-cart').click(function () {
        $(this).next().addClass('open');
    });
    /*Open menu end*/
    /*Close start*/
    $('.icon-close:not(.remove)').click(function () {
        $(this).parent().removeClass('open');
    });
    /*Close end*/
    /*Click body hide left menu, search, cart*/
    $(document).mouseup(function (e)  {
        var block = $(".search, .cart, .menu-block");
        if (!block.is(e.target) && block.has(e.target).length === 0) {
            block.removeClass('open');
        }
    });
    /*Click body hide left menu, search, cart*/
    /*Position arrow slider*/
    function positonArrow (){
            if($(window).width() > 768){
                var leftEl= $('.slick-slider .description').offset().left - 15,
                    topEl = $('.slick-slider .description').offset().top - 55;
            }else{
                var leftEl= $('.slick-slider .description').offset().left + 15,
                topEl = $('.slick-slider .description').offset().top - 55;
                
            }
        $('.icon-left').css({
            "top": topEl,
            "left": leftEl
        });
        $('.icon-right').css({
            "top": topEl,
            "left": leftEl + 65
        });
    }
    /*Position arrow slider*/
    /*Slider home page*/
    if($('.home-slider').length){
        if($(window).width() < 679){
            if (/apple/i.test(navigator.vendor)) {
                $('.home-slider .item, html, body, section').height(''+heightWindow+'px');
            }
        }
        $('.home-slider').slick({
            dots: true,
            infinite: true,
            speed: 500,
            fade: true,
            cssEase: 'linear',
            nextArrow: '<i class="icon-right"></i>',
            prevArrow: '<i class="icon-left"></i>',
        });
        positonArrow();
        $(window).on('resize', function () {
            if($(window).width() < 679){
                var heightWindow = $(window).height();
                if (/apple/i.test(navigator.vendor)) {
                    $('.home-slider .item, html, body, section').height(''+heightWindow+'px');
                }
            }else{
                $('.home-slider .item, html, body, section').height('100vh');
            }
            setTimeout(function(){
                positonArrow(); 
            },200)
        });
    }
    /*Slider home page*/
    /*Height Bio block mobile*/
    if($('.bio').length){
        if($(window).width() < 679){
            if (/apple/i.test(navigator.vendor)) {
                $('.bio .item, html, body, section').height(''+heightWindow+'px');
            } 
        }
        $(window).on('resize', function () {
            if($(window).width() < 679){
                var heightWindow = $(window).height();
                if (/apple/i.test(navigator.vendor)) {
                    $('.bio .item, html, body, section').height(''+heightWindow+'px');
                }
            }else{
                 $('.bio .item, body, html, section').height('100vh');
            }
        });
    }
    /*Height Bio block mobile*/
    /*Slider products*/
    if($('.products-slider').length){
        if($('.left-block').length){
            $('.products-slider').slick({
                dots: true,
                infinite: true,
                speed: 300,
                slidesToShow: 3,
                slidesToScroll: 3,
                variableWidth: true,
                responsive: [
                {
                    breakpoint: 1680,
                    settings: {
                        variableWidth: false,
                    }
                },
                {
                    breakpoint: 992,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2,
                        variableWidth: false,
                    }
                },
                {
                    breakpoint: 580,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        variableWidth: true,
                        dots:false,
                  }
                }
              ]
            });
        }else{
            $('.products-slider').slick({
                dots: true,
                infinite: true,
                speed: 300,
                slidesToShow: 3,
                slidesToScroll: 3,
                responsive: [
                {
                    breakpoint: 992,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2,
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2,
                    }
                },
                {
                    breakpoint: 580,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        variableWidth: true,
                        dots:false,
                  }
                }
              ]
            });
        }
        if($(window).width() < 560){
            $('.products .row, body, section').height(''+heightWindow+'px');
        }else{
            $('.products .row, body, section').height('100vh');
        }
        $(window).on('resize', function () {
            if($(window).width() < 560){
                var heightWindow = $(window).height();
                $('.products .row, body, section').height(''+heightWindow+'px');
            }else{
                 $('.products .row, body, section').height('100vh');
            }
        });
    }
    /*Slider products*/
    /*Slider product*/
    if($('.product-slider').length){ 
        if($('.product-slider .item').length > 1){
             $('.product-slider').slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: false,
                fade: true,
                asNavFor: '.product-slider-thumbnail',

            });
            $('.product-slider-thumbnail').slick({
                slidesToShow: 3,
                slidesToScroll: 1,
                asNavFor: '.product-slider',
                dots: false,
                centerMode: true,
                focusOnSelect: true,
                vertical: true,
                adaptiveHeight: true,
                centerPadding:'0',
                appendArrows:'.product-description',
                nextArrow: '<i class="icon-right"></i>',
                prevArrow: '<i class="icon-left"></i>',
                responsive: [
                    {
                        breakpoint: 1200,
                        settings: {
                            slidesToShow: 2,
                        }
                    }
                  ]
            });
        }
         if($(window).width() < 667){
            $('.single-product .products .row, body, section').css('height', ''+heightWindow+'px');
        }
        $(window).on('resize', function () {
            if($(window).width() < 667){
                var heightWindow = $(window).height();
                $('.single-product .products .row, body, section').css('height', ''+heightWindow+'px');
            }else{
                 $('.single-product .products .row, body, section').css('height','100vh');
            }
            var thumbnailSlider = $('.product-slider-thumbnail');
            if($(window).width() < 767){
                    $('.product-slider').before(thumbnailSlider);
            }else{
                 if($('.col-4[id*="product"] .product-slider-thumbnail').length == 0){
                    $('.price').after(thumbnailSlider);
                }
            }
            if($(window).height() < 480){
                if($(window).width() < 992){
                    $('.product-slider').before(thumbnailSlider);
                }else{
                    if($('.col-4[id*="product"] .product-slider-thumbnail').length == 0){
                        $('.price').after(thumbnailSlider);
                    }
                }
            }
        });
        if($(window).height() < 480){
            if($(window).width() < 992){
                var thumbnailSlider = $('.product-slider-thumbnail');
                $('.product-slider').before(thumbnailSlider);
            }
        }
         if($(window).width() < 767){
            $('.product-slider').before(thumbnailSlider);
        }
    }
    /*Slider product*/
    /*Select product*/
    if($('.select').length){
         $('.select select').select2({
             minimumResultsForSearch: Infinity
         });
    }
    /*Select product*/
    /*Input Number dialing only numbers*/
    $('[type="number"]').keypress(function(event){
		event = event || window.event;
		if (event.charCode && event.charCode!=0 && event.charCode!=46 && (event.charCode < 48 || event.charCode > 57) ){
			return false;
		}
	});
    /*Input Number dialing only numbers*/
    /*Input product quantity*/
    $('[type="number"]').each(function () {
        var min = $(this).attr("min"),
            max = $(this).attr("max"),
            el = $(this);
            if($('.woocommerce-cart').length){
                el.before('<span class="icon-minus decrement"></span>');
                el.after('<span class="icon-plus increment"></span>');
            }
            el.dec = $(this).prev();
            el.inc = $(this).next();
            el.keyup(function () {
                var value = el.val();
            })
        el.dec.mousedown(function () {
            var value = el[0].value;
            value--;
            if (!min || value >= min) {
                el[0].value = value;
            }
        });
        el.inc.mousedown(function () {
            var value = el[0].value;
            value++;
            if (!max || value <= max) {
                el[0].value = value++;
            }
        });
    });
    /*Input product quantity*/
    if($('.product').length){
        if($(window).width() < 768){
            $('.products-slider .item').each(function(){
                var priceBlock = $(this).find('.price-block');
                $(this).find('.button').before(priceBlock);
            });
        }
        $(window).on('resize', function () {
            if($(window).width() < 768){
                $('.products-slider .item').each(function(){
                    var priceBlock = $(this).find('.price-block');
                    $(this).find('.button').before(priceBlock);
                });
            }else{
                 $('.products-slider .item').each(function(){
                    var priceBlock = $(this).find('.price-block');
                    $(this).find('.choice').before(priceBlock);
                });
            }
        });
    }
    if($('.coupon').length){
        if($(window).width() < 992){
            $('.coupon button').addClass('icon-refresh');
        }
    }
    if($('.woocommerce-notices-wrapper').length){ 
        setTimeout(function(){
            $('.woocommerce-notices-wrapper, .woocommerce-form-coupon-toggle').hide();
        },5000);
    }
    if($('.woocommerce-checkout').length){
        $('.grid-col-checkout:nth-of-type(-n+2)').wrapAll('<div class="left-block-checkout"></div>');
        $('.input-text').each(function(){
            if($(this).attr('placeholder') == ""){
                $(this).parents('.form-row').addClass('no-placeholder');
            }
        });
        $('.input-text').focus(function(){
            $(this).parents('.form-row').addClass('focus');
        });
        $('.input-text').blur(function(){
            if($(this).val().length == 0){
                $(this).parents('.form-row').removeClass('focus');
            }
        });
        $('.input-text').each(function(){
            if($(this).val().length < 1){
                $(this).parents('.form-row').removeClass('focus');
            }else{
                $(this).parents('.form-row').addClass('focus');
            }
        });
    }
    $('.checkbox input').click(function(){
       if($(this).prop('checked') == true){ 
           $(this).parent().removeClass('checked');
       }else{
           $(this).parent().addClass('checked');
       }
    });
    if (/apple/i.test(navigator.vendor)) {
        $('body').addClass('ios');
    }
});
