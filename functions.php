<?php
/**
 * Ten Fajyne Theme.
 *
 * @package TEN
 */

use TEN\ThemeInit;

define( 'TEN_THEME_VERSTION', '1.2.0' );
define( 'TEN_THEME_URL', get_stylesheet_directory_uri() );
define( 'TEN_THEME_PATCH', get_stylesheet_directory() );

if ( ! function_exists( 'array_unique_recursive' ) ) {
	function array_unique_recursive( $array ) {
		$scalars = [];
		foreach ( $array as $key => $value ) {
			if ( is_scalar( $value ) ) {
				if ( isset( $scalars[ $value ] ) ) {
					unset( $array[ $key ] );
				} else {
					$scalars[ $value ] = true;
				}
			} elseif ( is_array( $value ) ) {
				$array[ $key ] = array_unique_recursive( $value );
			}
		}

		return $array;
	}
}

include_once __DIR__ . '/vendor/autoload.php';

new ThemeInit();
